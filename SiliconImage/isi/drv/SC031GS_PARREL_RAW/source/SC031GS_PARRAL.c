
/******************************************************************************
 *
 * Copyright 2010, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file SC031GS.c
 *
 * @brief
 *   ADD_DESCRIPTION_HERE
 *
 *****************************************************************************/
#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>
#include <common/misc.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"

#include "SC031GS_MIPI_priv.h"

#define  SC031GS_NEWEST_TUNING_XML "12-January-2018_Ethan_SC031GS_MTD2355-TVE1030G-V1.0"

#define CC_OFFSET_SCALING  2.0f
#define I2C_COMPLIANT_STARTBIT 1U

/******************************************************************************
 * local macro definitions
 *****************************************************************************/
CREATE_TRACER( SC031GS_INFO , "SC031GS: ", INFO,    1U );
CREATE_TRACER( SC031GS_WARN , "SC031GS: ", WARNING, 1U );
CREATE_TRACER( SC031GS_ERROR, "SC031GS: ", ERROR,   1U );

CREATE_TRACER( SC031GS_DEBUG, "SC031GS: ", ERROR,     1U );

CREATE_TRACER( SC031GS_REG_INFO , "SC031GS: ", INFO, 0);
CREATE_TRACER( SC031GS_REG_DEBUG, "SC031GS: ", INFO, 0U );

#define SC031GS_SLAVE_ADDR       0x60U                           /**< i2c slave address of the SC031GS camera sensor */
#define SC031GS_SLAVE_ADDR2      0x60U
#define SC031GS_SLAVE_AF_ADDR    0x00U                           /**< i2c slave address of the SC031GS integrated AF */

#define SC031GS_MIN_GAIN_STEP   	( 1.0f / 16.0f);		/**< min gain step size used by GUI ( 32/(32-7) - 32/(32-6); min. reg value is 6 as of datasheet; depending on actual gain ) */		 
#define SC031GS_MAX_GAIN_AEC    	( 64.0f ) 				/**< max. gain used by the AEC (arbitrarily chosen) */


/*!<
 * Focus position values:
 * 65 logical positions ( 0 - 64 )
 * where 64 is the setting for infinity and 0 for macro
 * corresponding to
 * 1024 register settings (0 - 1023)
 * where 0 is the setting for infinity and 1023 for macro
 */
#define MAX_LOG   64U
#define MAX_REG 1023U

#define MAX_VCMDRV_CURRENT      100U
#define MAX_VCMDRV_REG          1023U


/*!<
 * Lens movement is triggered every 133ms (VGA, 7.5fps processed frames
 * worst case assumed, usually even much slower, see OV5630 driver for
 * details). Thus the lens has to reach the requested position after
 * max. 133ms. Minimum mechanical ringing is expected with mode 1 ,
 * 100us per step. A movement over the full range needs max. 102.3ms
 * (see table 9 AD5820 datasheet).
 */
#define MDI_SLEW_RATE_CTRL 6U /* S3..0 */



/******************************************************************************
 * local variable declarations
 *****************************************************************************/
const char SC031GS_g_acName[] = "SC031GS_PARRAL";

extern const IsiRegDescription_t SC031GS_g_aRegDescription[];
extern const IsiRegDescription_t SC031GS_g_svga[];
extern const IsiRegDescription_t SC031GS_g_640x480[];
extern const IsiRegDescription_t SC031GS_g_640x480_30fps[];
extern const IsiRegDescription_t SC031GS_g_640x480_20fps[];
extern const IsiRegDescription_t SC031GS_g_640x480_15fps[];
extern const IsiRegDescription_t SC031GS_g_640x480_10fps[];


const IsiSensorCaps_t SC031GS_g_IsiSensorDefaultConfig;


#define SC031GS_I2C_START_BIT        (I2C_COMPLIANT_STARTBIT)    // I2C bus start condition
#define SC031GS_I2C_NR_ADR_BYTES     (1U)                        // 1 byte base address and 2 bytes sub address
#define SC031GS_I2C_NR_DAT_BYTES     (1U)                        // 8 bit registers


/******************************************************************************
 * local function prototypes
 *****************************************************************************/
static RESULT SC031GS_IsiCreateSensorIss( IsiSensorInstanceConfig_t *pConfig );
static RESULT SC031GS_IsiReleaseSensorIss( IsiSensorHandle_t handle );
static RESULT SC031GS_IsiGetCapsIss( IsiSensorHandle_t handle, IsiSensorCaps_t *pIsiSensorCaps );
static RESULT SC031GS_IsiSetupSensorIss( IsiSensorHandle_t handle, const IsiSensorConfig_t *pConfig );
static RESULT SC031GS_IsiSensorSetStreamingIss( IsiSensorHandle_t handle, bool_t on );
static RESULT SC031GS_IsiSensorSetPowerIss( IsiSensorHandle_t handle, bool_t on );
static RESULT SC031GS_IsiCheckSensorConnectionIss( IsiSensorHandle_t handle );
static RESULT SC031GS_IsiGetSensorRevisionIss( IsiSensorHandle_t handle, uint32_t *p_value);

static RESULT SC031GS_IsiGetGainLimitsIss( IsiSensorHandle_t handle, float *pMinGain, float *pMaxGain);
static RESULT SC031GS_IsiGetIntegrationTimeLimitsIss( IsiSensorHandle_t handle, float *pMinIntegrationTime, float *pMaxIntegrationTime );
static RESULT SC031GS_IsiExposureControlIss( IsiSensorHandle_t handle, float NewGain, float NewIntegrationTime, uint8_t *pNumberOfFramesToSkip, float *pSetGain, float *pSetIntegrationTime );
static RESULT SC031GS_IsiGetCurrentExposureIss( IsiSensorHandle_t handle, float *pSetGain, float *pSetIntegrationTime );
static RESULT SC031GS_IsiGetAfpsInfoIss ( IsiSensorHandle_t handle, uint32_t Resolution, IsiAfpsInfo_t* pAfpsInfo);
static RESULT SC031GS_IsiGetGainIss( IsiSensorHandle_t handle, float *pSetGain );
static RESULT SC031GS_IsiGetGainIncrementIss( IsiSensorHandle_t handle, float *pIncr );
static RESULT SC031GS_IsiSetGainIss( IsiSensorHandle_t handle, float NewGain, float *pSetGain, uint8_t *pNumberOfFramesToSkip );
static RESULT SC031GS_IsiGetIntegrationTimeIss( IsiSensorHandle_t handle, float *pSetIntegrationTime );
static RESULT SC031GS_IsiGetIntegrationTimeIncrementIss( IsiSensorHandle_t handle, float *pIncr );
static RESULT SC031GS_IsiSetIntegrationTimeIss( IsiSensorHandle_t handle, float NewIntegrationTime, float *pSetIntegrationTime, uint8_t *pNumberOfFramesToSkip );
static RESULT SC031GS_IsiGetResolutionIss( IsiSensorHandle_t handle, uint32_t *pSetResolution );


static RESULT SC031GS_IsiRegReadIss( IsiSensorHandle_t handle, const uint32_t address, uint32_t *p_value );
static RESULT SC031GS_IsiRegWriteIss( IsiSensorHandle_t handle, const uint32_t address, const uint32_t value );

static RESULT SC031GS_IsiGetCalibKFactor( IsiSensorHandle_t handle, Isi1x1FloatMatrix_t **pIsiKFactor );
static RESULT SC031GS_IsiGetCalibPcaMatrix( IsiSensorHandle_t   handle, Isi3x2FloatMatrix_t **pIsiPcaMatrix );
static RESULT SC031GS_IsiGetCalibSvdMeanValue( IsiSensorHandle_t   handle, Isi3x1FloatMatrix_t **pIsiSvdMeanValue );
static RESULT SC031GS_IsiGetCalibCenterLine( IsiSensorHandle_t   handle, IsiLine_t  **ptIsiCenterLine);
static RESULT SC031GS_IsiGetCalibClipParam( IsiSensorHandle_t   handle, IsiAwbClipParm_t    **pIsiClipParam );
static RESULT SC031GS_IsiGetCalibGlobalFadeParam( IsiSensorHandle_t       handle, IsiAwbGlobalFadeParm_t  **ptIsiGlobalFadeParam);
static RESULT SC031GS_IsiGetCalibFadeParam( IsiSensorHandle_t   handle, IsiAwbFade2Parm_t   **ptIsiFadeParam);
static RESULT SC031GS_IsiGetIlluProfile( IsiSensorHandle_t   handle, const uint32_t CieProfile, IsiIlluProfile_t **ptIsiIlluProfile );

static RESULT SC031GS_IsiMdiInitMotoDriveMds( IsiSensorHandle_t handle );
static RESULT SC031GS_IsiMdiSetupMotoDrive( IsiSensorHandle_t handle, uint32_t *pMaxStep );
static RESULT SC031GS_IsiMdiFocusSet( IsiSensorHandle_t handle, const uint32_t Position );
static RESULT SC031GS_IsiMdiFocusGet( IsiSensorHandle_t handle, uint32_t *pAbsStep );
static RESULT SC031GS_IsiMdiFocusCalibrate( IsiSensorHandle_t handle );

static RESULT SC031GS_IsiGetSensorMipiInfoIss( IsiSensorHandle_t handle, IsiSensorMipiInfo *ptIsiSensorMipiInfo);
static RESULT SC031GS_IsiGetSensorIsiVersion(  IsiSensorHandle_t   handle, unsigned int* pVersion);
static RESULT SC031GS_IsiGetSensorTuningXmlVersion(  IsiSensorHandle_t   handle, char** pTuningXmlVersion);
static RESULT SC031GS_IsiSetSensorFrameRateLimit(IsiSensorHandle_t handle, uint32_t minimum_framerate);
static RESULT SC031GS_IsiGetColorIss(IsiSensorHandle_t   handle, char	*pGetColor);

static float dctfloor( const float f )
{
    if ( f < 0 )
    {
        return ( (float)((int32_t)f - 1L) );
    }
    else
    {
        return ( (float)((uint32_t)f) );
    }
}



/*****************************************************************************/
/**
 *          SC031GS_IsiCreateSensorIss
 *
 * @brief   This function creates a new OV13850 sensor instance handle.
 *
 * @param   pConfig     configuration structure to create the instance
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 * @retval  RET_OUTOFMEM
 *
 *****************************************************************************/
static RESULT SC031GS_IsiCreateSensorIss
(
    IsiSensorInstanceConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;
	int32_t current_distance;
    SC031GS_Context_t *pSensorCtx;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( (pConfig == NULL) || (pConfig->pSensor ==NULL) )
    {
        return ( RET_NULL_POINTER );
    }

    pSensorCtx = ( SC031GS_Context_t * )malloc ( sizeof (SC031GS_Context_t) );
    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR,  "%s: Can't allocate SC031GS context\n",  __FUNCTION__ );
        return ( RET_OUTOFMEM );
    }
    MEMSET( pSensorCtx, 0, sizeof( SC031GS_Context_t ) );

    result = HalAddRef( pConfig->HalHandle );
    if ( result != RET_SUCCESS )
    {
        free ( pSensorCtx );
        return ( result );
    }

    pSensorCtx->IsiCtx.HalHandle              = pConfig->HalHandle;
    pSensorCtx->IsiCtx.HalDevID               = pConfig->HalDevID;
    pSensorCtx->IsiCtx.I2cBusNum              = pConfig->I2cBusNum;
    pSensorCtx->IsiCtx.SlaveAddress           = ( pConfig->SlaveAddr == 0 ) ? SC031GS_SLAVE_ADDR : pConfig->SlaveAddr;
    pSensorCtx->IsiCtx.NrOfAddressBytes       = 2U;

    pSensorCtx->IsiCtx.I2cAfBusNum            = pConfig->I2cAfBusNum;
    pSensorCtx->IsiCtx.SlaveAfAddress         = ( pConfig->SlaveAfAddr == 0 ) ? SC031GS_SLAVE_AF_ADDR : pConfig->SlaveAfAddr;
    pSensorCtx->IsiCtx.NrOfAfAddressBytes     = 0U;

    pSensorCtx->IsiCtx.pSensor                = pConfig->pSensor;

    pSensorCtx->Configured             = BOOL_FALSE;
    pSensorCtx->Streaming              = BOOL_FALSE;
    pSensorCtx->TestPattern            = BOOL_FALSE;
    pSensorCtx->isAfpsRun              = BOOL_FALSE;
    /* ddl@rock-chips.com: v0.3.0 */
    current_distance = pConfig->VcmRatedCurrent - pConfig->VcmStartCurrent;
    current_distance = current_distance*MAX_VCMDRV_REG/MAX_VCMDRV_CURRENT;
    pSensorCtx->VcmInfo.Step = (current_distance+(MAX_LOG-1))/MAX_LOG;
    pSensorCtx->VcmInfo.StartCurrent   = pConfig->VcmStartCurrent*MAX_VCMDRV_REG/MAX_VCMDRV_CURRENT;
    pSensorCtx->VcmInfo.RatedCurrent   = pSensorCtx->VcmInfo.StartCurrent + MAX_LOG*pSensorCtx->VcmInfo.Step;
    pSensorCtx->VcmInfo.StepMode       = pConfig->VcmStepMode;

    pConfig->hSensor = ( IsiSensorHandle_t )pSensorCtx;

    result = HalSetCamConfig( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, false, true, false );
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    result = HalSetClock( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, 24000000U);
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiReleaseSensorIss
 *
 * @brief   This function destroys/releases an Sensor instance.
 *
 * @param   handle      Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 *
 *****************************************************************************/
static RESULT SC031GS_IsiReleaseSensorIss
(
    IsiSensorHandle_t handle
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    (void)SC031GS_IsiSensorSetStreamingIss( pSensorCtx, BOOL_FALSE );
    (void)SC031GS_IsiSensorSetPowerIss( pSensorCtx, BOOL_FALSE );

    (void)HalDelRef( pSensorCtx->IsiCtx.HalHandle );

    MEMSET( pSensorCtx, 0, sizeof( SC031GS_Context_t ) );
    free ( pSensorCtx );

    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCapsIss
 *
 * @brief   fills in the correct pointers for the sensor description struct
 *
 * @param   param1      pointer to sensor capabilities structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCapsIssInternal
(
    IsiSensorCaps_t   *pIsiSensorCaps
)
{

    RESULT result = RET_SUCCESS;
	TRACE( SC031GS_INFO, "%s(%d) enter ....\n", __FUNCTION__, __LINE__);
    if ( pIsiSensorCaps == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {
        switch (pIsiSensorCaps->Index) 
        {

            case 0:
            {
                pIsiSensorCaps->Resolution = ISI_RES_VGAP30;
                break;
            }
#if 1     
            case 1:
            {
                pIsiSensorCaps->Resolution = ISI_RES_VGAP20;
                break;
            }
            case 2:
            {
                pIsiSensorCaps->Resolution = ISI_RES_VGAP15;
                break;
            }

            case 3:
            {
                pIsiSensorCaps->Resolution = ISI_RES_VGAP10;
                break;
            }
#endif            
			/*
            case 1:
            {
                pIsiSensorCaps->Resolution = ISI_RES_SVGAP30;
                break;
            }
			*/
            default:
            {
                result = RET_OUTOFRANGE;
                goto end;
            }

        }            

    
        pIsiSensorCaps->BusWidth        = ISI_BUSWIDTH_10BIT;
        pIsiSensorCaps->Mode            = ISI_MODE_PICT|ISI_MODE_BT601;
        pIsiSensorCaps->FieldSelection  = ISI_FIELDSEL_BOTH;
        pIsiSensorCaps->YCSequence      = ISI_YCSEQ_YCBYCR;           /**< only Bayer supported, will not be evaluated */
        pIsiSensorCaps->Conv422         = ISI_CONV422_NOCOSITED;
        pIsiSensorCaps->BPat            = ISI_BPAT_RGRGGBGB;
        pIsiSensorCaps->HPol            = ISI_HPOL_REFPOS;
        pIsiSensorCaps->VPol            = ISI_VPOL_POS;
        pIsiSensorCaps->Edge            = ISI_EDGE_RISING;
        pIsiSensorCaps->Bls             = ISI_BLS_OFF;
        pIsiSensorCaps->Gamma           = ISI_GAMMA_OFF;
        pIsiSensorCaps->CConv           = ISI_CCONV_OFF;
        pIsiSensorCaps->BLC             = ( ISI_BLC_AUTO);
        pIsiSensorCaps->AGC             = ( ISI_AGC_OFF );
        pIsiSensorCaps->AWB             = ( ISI_AWB_OFF );
        pIsiSensorCaps->AEC             = ( ISI_AEC_OFF );
        pIsiSensorCaps->DPCC            = ( ISI_DPCC_OFF );

        pIsiSensorCaps->DwnSz           = ISI_DWNSZ_SUBSMPL;
        pIsiSensorCaps->CieProfile      = 0;
        pIsiSensorCaps->SmiaMode        = ISI_SMIA_OFF;
        pIsiSensorCaps->MipiMode        = ISI_MIPI_OFF;
        pIsiSensorCaps->AfpsResolutions = ( ISI_AFPS_NOTSUPP );
		pIsiSensorCaps->SensorOutputMode = ISI_SENSOR_OUTPUT_MODE_RAW;
    }
end:

    return ( result );
}

static RESULT SC031GS_IsiGetCapsIss
(
    IsiSensorHandle_t handle,
    IsiSensorCaps_t   *pIsiSensorCaps
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    result = SC031GS_IsiGetCapsIssInternal(pIsiSensorCaps);
    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_g_IsiSensorDefaultConfig
 *
 * @brief   recommended default configuration for application use via call
 *          to IsiGetSensorIss()
 *
 *****************************************************************************/
const IsiSensorCaps_t SC031GS_g_IsiSensorDefaultConfig =
{
    ISI_BUSWIDTH_10BIT,         // BusWidth
    ISI_MODE_BAYER,             //ISI_MODE_BAY_BT656,//ISI_MODE_BAYER, // Bayer data with separate h/v sync lines
    ISI_FIELDSEL_BOTH,          // FieldSel
    ISI_YCSEQ_YCBYCR,           // YCSeq
    ISI_CONV422_NOCOSITED,      // Conv422
    ISI_BPAT_RGRGGBGB,          // BPat
    ISI_HPOL_REFPOS,            // HPol
    ISI_VPOL_POS,               // VPol
    ISI_EDGE_RISING,            // Edge
    ISI_BLS_OFF,                // Bls
    ISI_GAMMA_OFF,              // Gamma
    ISI_CCONV_OFF,              // CConv
    ISI_RES_VGAP30,            // Res
    ISI_DWNSZ_SUBSMPL,          // DwnSz
    ISI_BLC_AUTO,               // BLC
    ISI_AGC_OFF,                // AGC
    ISI_AWB_OFF,                // AWB
    ISI_AEC_OFF,                // AEC
    ISI_DPCC_OFF,               // DPCC
    ISI_CIEPROF_D65,            // CieProfile, this is also used as start profile for AWB (if not altered by menu settings)
    ISI_SMIA_OFF,               // SmiaMode
    ISI_MIPI_OFF,       		// MipiMode
    ISI_AFPS_NOTSUPP,           // AfpsResolutions
    ISI_SENSOR_OUTPUT_MODE_RAW,
    0,
};



/*****************************************************************************/
/**
 *          SC031GS_SetupOutputFormat
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      Sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_SetupOutputFormat
(
    SC031GS_Context_t       *pSensorCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s(%d) (enter)\n", __FUNCTION__, __LINE__);

    /* bus-width */
    switch ( pConfig->BusWidth )        /* only ISI_BUSWIDTH_12BIT supported, no configuration needed here */
    {
        case ISI_BUSWIDTH_10BIT:
        case ISI_BUSWIDTH_8BIT_ZZ:
        case ISI_BUSWIDTH_12BIT:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: bus width not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* mode */
    switch ( pConfig->Mode )            /* only ISI_MODE_BAYER supported, no configuration needed here */
    {
        case ISI_MODE_MIPI :
        case ISI_MODE_BAYER:
        case ISI_MODE_BT601:
        case ISI_MODE_PICT:
        case ISI_MODE_DATA:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: mode not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* field-selection */
    switch ( pConfig->FieldSelection )  /* only ISI_FIELDSEL_BOTH supported, no configuration needed */
    {
        case ISI_FIELDSEL_BOTH:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: field selection not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* only Bayer mode is supported by Sensor, so the YCSequence parameter is not checked */
    switch ( pConfig->YCSequence )
    {
        default:
        {
            break;
        }
    }

    /* 422 conversion */
    switch ( pConfig->Conv422 )         /* only ISI_CONV422_NOCOSITED supported, no configuration needed */
    {
        case ISI_CONV422_NOCOSITED:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: 422 conversion not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* bayer-pattern */
    switch ( pConfig->BPat )            /* only ISI_BPAT_BGBGGRGR supported, no configuration needed */
    {
		case ISI_BPAT_RGRGGBGB:
		case ISI_BPAT_GRGRBGBG:
		case ISI_BPAT_GBGBRGRG:
		case ISI_BPAT_BGBGGRGR:
		{
            break;
        }
        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: bayer pattern not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* horizontal polarity */
    switch ( pConfig->HPol )            /* only ISI_HPOL_REFPOS supported, no configuration needed */
    {
        case ISI_HPOL_REFPOS:
		case ISI_HPOL_REFNEG:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: HPol not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* vertical polarity */
    switch ( pConfig->VPol )            /*no configuration needed */
    {
        case ISI_VPOL_NEG:
        {
            break;
        }
        case ISI_VPOL_POS:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: VPol not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }


    /* edge */
    switch ( pConfig->Edge )            /* only ISI_EDGE_RISING supported, no configuration needed */
    {
        case ISI_EDGE_RISING:
        {
            break;
        }

        case ISI_EDGE_FALLING:          /*TODO for MIPI debug*/
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s:  edge mode not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* gamma */
    switch ( pConfig->Gamma )           /* only ISI_GAMMA_OFF supported, no configuration needed */
    {
        case ISI_GAMMA_ON:
        case ISI_GAMMA_OFF:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s:  gamma not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* color conversion */
    switch ( pConfig->CConv )           /* only ISI_CCONV_OFF supported, no configuration needed */
    {
        case ISI_CCONV_OFF:
        case ISI_CCONV_ON:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: color conversion not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->SmiaMode )        /* only ISI_SMIA_OFF supported, no configuration needed */
    {
        case ISI_SMIA_OFF:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: SMIA mode not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->MipiMode )        /* only ISI_MIPI_MODE_RAW_12 supported, no configuration needed */
    {
        case ISI_MIPI_MODE_RAW_10:
		case ISI_MIPI_OFF:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s%s: MIPI mode not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->AfpsResolutions ) /* no configuration needed */
    {
        case ISI_AFPS_NOTSUPP:
        {
            break;
        }
        default:
        {
            // don't care about what comes in here
            TRACE( SC031GS_ERROR, "%s%s: AFPS not supported\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"" );
            //return ( RET_NOTSUPP );
        }
    }

    TRACE( SC031GS_ERROR, "%s%s (exit)\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"");

    return ( result );
}

int SC031GS_get_PCLK( SC031GS_Context_t *pSensorCtx, int XVCLK)
{
	// calculate sysclk
	uint32_t sysclk,divx4,pre_div,temp1,temp2;
	RESULT result = RET_SUCCESS;

	TRACE( SC031GS_INFO, "%s(%d): (enter)\n", __FUNCTION__, __LINE__);

	return sysclk;
 }

/*****************************************************************************/
/**
 *          SC031GS_SetupOutputWindow
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      Sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_SetupOutputWindowInternal
(
    SC031GS_Context_t        *pSensorCtx,
    const IsiSensorConfig_t *pConfig,
    bool_t set2Sensor,
    bool_t res_no_chg
)
{
    RESULT result     = RET_SUCCESS;
    uint16_t usFrameLengthLines = 0;
    uint16_t usLineLengthPck    = 0;
    float    rVtPixClkFreq      = 0.0f;
    int xclk = 24000000;

    TRACE( SC031GS_INFO, "%s (enter)---pConfig->Resolution:%x\n", __FUNCTION__,pConfig->Resolution);


    /* resolution */
    switch ( pConfig->Resolution )
    {
		/*case ISI_RES_SVGAP30:
        {
		  	if((result = IsiRegDefaultsApply((IsiSensorHandle_t)pSensorCtx, SC031GS_g_svga)) != RET_SUCCESS){
				result = RET_FAILURE;
				TRACE( SC031GS_ERROR, "%s: failed to set one lane ISI_RES_640_480 \n", __FUNCTION__ );
            }

            usLineLengthPck = 0x06e8;
            usFrameLengthLines = 0x04dc;
			pSensorCtx->IsiSensorMipiInfo.ulMipiFreq = 330;
            break;
            
        }*/
        case ISI_RES_VGAP30:
		case ISI_RES_VGAP20:
		case ISI_RES_VGAP15:
		case ISI_RES_VGAP10:
        {
            if (set2Sensor == BOOL_TRUE) {                    
                if (res_no_chg == BOOL_FALSE) {
					if((result = IsiRegDefaultsApply((IsiSensorHandle_t)pSensorCtx, SC031GS_g_640x480)) != RET_SUCCESS){
						result = RET_FAILURE;
						TRACE( SC031GS_ERROR, "%s: failed to set one lane SC031GS_g_640x480 \n", __FUNCTION__ );
		            }
				}
                

                if (pConfig->Resolution == ISI_RES_VGAP30) {						 
					result = IsiRegDefaultsApply( (IsiSensorHandle_t)pSensorCtx, SC031GS_g_640x480_30fps);
				}else if (pConfig->Resolution == ISI_RES_VGAP20) {
					result = IsiRegDefaultsApply( (IsiSensorHandle_t)pSensorCtx, SC031GS_g_640x480_20fps);
				}else if (pConfig->Resolution == ISI_RES_VGAP15) {
					result = IsiRegDefaultsApply( (IsiSensorHandle_t)pSensorCtx, SC031GS_g_640x480_15fps);
				}else if (pConfig->Resolution == ISI_RES_VGAP10) {
					result = IsiRegDefaultsApply( (IsiSensorHandle_t)pSensorCtx, SC031GS_g_640x480_10fps);
				}  
            }

            usLineLengthPck = 0x36e;
			//have to reset mipi freq here
			//pSensorCtx->IsiSensorMipiInfo.ulMipiFreq = 580; //680; //620; //480; //580; //528; //328;

			if (pConfig->Resolution == ISI_RES_VGAP30) {				
				TRACE( SC031GS_ERROR, "%s: ISI_RES_VGAP30 \n", __FUNCTION__ );
                usFrameLengthLines = 0xaac;	
			}else if(pConfig->Resolution == ISI_RES_VGAP20) {
				TRACE( SC031GS_ERROR, "%s: ISI_RES_VGAP20 \n", __FUNCTION__ );                    
            	usFrameLengthLines = 0x1002; 
			}else if(pConfig->Resolution == ISI_RES_VGAP15) {				
				TRACE( SC031GS_ERROR, "%s: ISI_RES_VGAP15 \n", __FUNCTION__ );
            	usFrameLengthLines = 0x1558; 
			}else if(pConfig->Resolution == ISI_RES_VGAP10) {
				TRACE( SC031GS_ERROR, "%s: ISI_RES_VGAP10 \n", __FUNCTION__ );
            	usFrameLengthLines = 0x2004;       //100ms
                //usFrameLengthLines = 0xe12f4;       //130ms
                //usFrameLengthLines = 0xe1d28;  //200ms
			}
            break; 
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: one lane Resolution not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }

	// store frame timing for later use in AEC module
	rVtPixClkFreq = 72000000;//SC031GS_get_PCLK(pSensorCtx, xclk);
    pSensorCtx->VtPixClkFreq     = rVtPixClkFreq;
	TRACE( SC031GS_INFO, "%s: rVtPixClkFreq = %f++++++\n", __FUNCTION__,rVtPixClkFreq );
    pSensorCtx->LineLengthPck    = usLineLengthPck;
    pSensorCtx->FrameLengthLines = usFrameLengthLines;	
	pSensorCtx->AecMaxIntegrationTime = ( ((float)pSensorCtx->FrameLengthLines) * ((float)pSensorCtx->LineLengthPck) )/ pSensorCtx->VtPixClkFreq;
    TRACE( SC031GS_DEBUG, "%s ethan AecMaxIntegrationTime:%f(****************exit): Resolution %dx%d@%dfps  MIPI %dlanes  res_no_chg: %d   rVtPixClkFreq: %f\n", __FUNCTION__,
    					pSensorCtx->AecMaxIntegrationTime,
                        ISI_RES_W_GET(pConfig->Resolution),ISI_RES_H_GET(pConfig->Resolution),
                        ISI_FPS_GET(pConfig->Resolution),
                        pSensorCtx->IsiSensorMipiInfo.ucMipiLanes,
                        res_no_chg,rVtPixClkFreq);


    return ( result );
}




/*****************************************************************************/
/**
 *          SC031GS_SetupImageControl
 *
 * @brief   Sets the image control functions (BLC, AGC, AWB, AEC, DPCC ...)
 *
 * @param   handle      Sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_SetupImageControl
(
    SC031GS_Context_t        *pSensorCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;

    uint32_t RegValue = 0U;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    switch ( pConfig->Bls )      /* only ISI_BLS_OFF supported, no configuration needed */
    {
        case ISI_BLS_OFF:
        {
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: Black level not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }

    /* black level compensation */
    switch ( pConfig->BLC )
    {
        case ISI_BLC_OFF:
        {
            /* turn off black level correction (clear bit 0) */
            //result = SC031GS_IsiRegReadIss(  pSensorCtx, SC031GS_BLC_CTRL00, &RegValue );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_BLC_CTRL00, RegValue & 0x7F);
            break;
        }

        case ISI_BLC_AUTO:
        {
            /* turn on black level correction (set bit 0)
             * (0x331E[7] is assumed to be already setup to 'auto' by static configration) */
            //result = SC031GS_IsiRegReadIss(  pSensorCtx, SC031GS_BLC_CTRL00, &RegValue );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_BLC_CTRL00, RegValue | 0x80 );
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: BLC not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }

    /* automatic gain control */
    switch ( pConfig->AGC )
    {
        case ISI_AGC_OFF:
        {
            // manual gain (appropriate for AEC with Marvin)
            //result = SC031GS_IsiRegReadIss(  pSensorCtx, SC031GS_AEC_MANUAL, &RegValue );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_AEC_MANUAL, RegValue | 0x02 );
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: AGC not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }

    /* automatic white balance */
    switch( pConfig->AWB )
    {
        case ISI_AWB_OFF:
        {
            //result = SC031GS_IsiRegReadIss(  pSensorCtx, SC031GS_ISP_CTRL01, &RegValue );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_ISP_CTRL01, RegValue | 0x01 );
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: AWB not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }

    switch( pConfig->AEC )
    {
        case ISI_AEC_OFF:
        {
            //result = SC031GS_IsiRegReadIss(  pSensorCtx, SC031GS_AEC_MANUAL, &RegValue );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_AEC_MANUAL, RegValue | 0x01 );
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: AEC not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }


    switch( pConfig->DPCC )
    {
        case ISI_DPCC_OFF:
        {
            // disable white and black pixel cancellation (clear bit 6 and 7)
            //result = SC031GS_IsiRegReadIss( pSensorCtx, SC031GS_ISP_CTRL00, &RegValue );
            //RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
            //result = SC031GS_IsiRegWriteIss( pSensorCtx, SC031GS_ISP_CTRL00, (RegValue &0x7c) );
            //RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
            break;
        }

        default:
        {
            TRACE( SC031GS_ERROR, "%s: DPCC not supported\n", __FUNCTION__ );
            return ( RET_NOTSUPP );
        }
    }// I have not update this commented part yet, as I did not find DPCC setting in the current 8810 driver of Trillian board. - SRJ

    return ( result );
}
static RESULT SC031GS_SetupOutputWindow
(
    SC031GS_Context_t        *pSensorCtx,
    const IsiSensorConfig_t *pConfig    
)
{
    bool_t res_no_chg;

    if ((ISI_RES_W_GET(pConfig->Resolution)==ISI_RES_W_GET(pSensorCtx->Config.Resolution)) && 
        (ISI_RES_W_GET(pConfig->Resolution)==ISI_RES_W_GET(pSensorCtx->Config.Resolution))) {
        res_no_chg = BOOL_TRUE;
        
    } else {
        res_no_chg = BOOL_FALSE;
    }

    return SC031GS_SetupOutputWindowInternal(pSensorCtx,pConfig,BOOL_TRUE, BOOL_FALSE);
}


/*****************************************************************************/
/**
 *          SC031GS_AecSetModeParameters
 *
 * @brief   This function fills in the correct parameters in Sensor-Instances
 *          according to AEC mode selection in IsiSensorConfig_t.
 *
 * @note    It is assumed that IsiSetupOutputWindow has been called before
 *          to fill in correct values in instance structure.
 *
 * @param   handle      Sensor context
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_AecSetModeParameters
(
    SC031GS_Context_t       *pSensorCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;
	//return ( result );
    //TRACE( SC031GS_INFO, "%s%s (enter)\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"");
    TRACE( SC031GS_INFO, "%s%s (enter)  Res: 0x%x  0x%x\n", __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"AFPS Not run",
        pSensorCtx->Config.Resolution, pConfig->Resolution);

    if ( (pSensorCtx->VtPixClkFreq == 0.0f) )
    {
        TRACE( SC031GS_ERROR, "%s%s: Division by zero!\n", __FUNCTION__  );
        return ( RET_OUTOFRANGE );
    }

    // (formula is usually MaxIntTime = (CoarseMax * LineLength + FineMax) / Clk
    //                     MinIntTime = (CoarseMin * LineLength + FineMin) / Clk )
    pSensorCtx->AecMaxIntegrationTime = ( ((float)(pSensorCtx->FrameLengthLines)) * ((float)pSensorCtx->LineLengthPck) )/ pSensorCtx->VtPixClkFreq;
    pSensorCtx->AecMinIntegrationTime = 0.0001f;

    pSensorCtx->AecMaxGain = SC031GS_MAX_GAIN_AEC;
    pSensorCtx->AecMinGain = 1.0f; //as of sensor datasheet 32/(32-6)

    //_smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application)
    pSensorCtx->AecIntegrationTimeIncrement = ((float)pSensorCtx->LineLengthPck) / pSensorCtx->VtPixClkFreq;
    pSensorCtx->AecGainIncrement = SC031GS_MIN_GAIN_STEP;

    //reflects the state of the sensor registers, must equal default settings
    pSensorCtx->AecCurGain               = pSensorCtx->AecMinGain;
    pSensorCtx->AecCurIntegrationTime    = 0.0f;
    pSensorCtx->OldCoarseIntegrationTime = 0;
    pSensorCtx->OldFineIntegrationTime   = 0;
	//pSensorCtx->GroupHold                = true; //must be true (for unknown reason) to correctly set gain the first time

    TRACE( SC031GS_DEBUG, "%s%s (exit) ethan pSensorCtx->AecMaxIntegrationTime:%f, pSensorCtx->FrameLengthLines:%d, pSensorCtx->LineLengthPck:%d,pSensorCtx->VtPixClkFreq:%f\n",
    __FUNCTION__, pSensorCtx->isAfpsRun?"(AFPS)":"",
    pSensorCtx->AecMaxIntegrationTime,
    pSensorCtx->FrameLengthLines,
    pSensorCtx->LineLengthPck,
    pSensorCtx->VtPixClkFreq
    );
	TRACE( SC031GS_INFO, "%s(%d) (exit)\n", __FUNCTION__, __LINE__);
    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiSetupSensorIss
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      Sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiSetupSensorIss
(
    IsiSensorHandle_t       handle,
    const IsiSensorConfig_t *pConfig
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    uint32_t RegValue = 0;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pConfig == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid configuration (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_NULL_POINTER );
    }

    if ( pSensorCtx->Streaming != BOOL_FALSE )
    {
        return RET_WRONG_STATE;
    }

    MEMCPY( &pSensorCtx->Config, pConfig, sizeof( IsiSensorConfig_t ) );

    /* 1.) SW reset of image sensor (via I2C register interface),  Bit[7]software_reset 1:soft reset 0: normal mode */
    result = SC031GS_IsiRegWriteIss ( pSensorCtx, SC031GS_SOFTWARE_RST, 0x01u );
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    osSleep( 10 );
    
    /* 2.) write default values derived from datasheet and evaluation kit (static setup altered by dynamic setup further below) */
	result = IsiRegDefaultsApply( pSensorCtx, SC031GS_g_aRegDescription);
    
    if ( result != RET_SUCCESS )
    {
        return ( result );
    }

    /* sleep a while, that sensor can take over new default values */
    osSleep( 10 );

    /* 3.) verify default values to make sure everything has been written correctly as expected */
	#if 0
	result = IsiRegDefaultsVerify( pSensorCtx, SC031GS_g_aRegDescription );
    if ( result != RET_SUCCESS )
    {
        return ( result );
    }
	#endif

    /* 4.) setup output format (RAW10|RAW12) */
    result = SC031GS_SetupOutputFormat( pSensorCtx, pConfig );
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: SetupOutputFormat failed.\n", __FUNCTION__);
        return ( result );
    }

    /* 5.) setup output window */
    result = SC031GS_SetupOutputWindow( pSensorCtx, pConfig );
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: SetupOutputWindow failed.\n", __FUNCTION__);
        return ( result );
    }

    result = SC031GS_SetupImageControl( pSensorCtx, pConfig );
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: SetupImageControl failed.\n", __FUNCTION__);
        return ( result );
    }

    result = SC031GS_AecSetModeParameters( pSensorCtx, pConfig );
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: AecSetModeParameters failed.\n", __FUNCTION__);
        return ( result );
    }
    if (result == RET_SUCCESS)
    {
        pSensorCtx->Configured = BOOL_TRUE;
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiChangeSensorResolutionIss
 *
 * @brief   Change image sensor resolution while keeping all other static settings.
 *          Dynamic settings like current gain & integration time are kept as
 *          close as possible. Sensor needs 2 frames to engage (first 2 frames
 *          are not correctly exposed!).
 *
 * @note    Re-read current & min/max values as they will probably have changed!
 *
 * @param   handle                  Sensor instance handle
 * @param   Resolution              new resolution ID
 * @param   pNumberOfFramesToSkip   reference to storage for number of frames to skip
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_WRONG_STATE
 * @retval  RET_OUTOFRANGE
 *
 *****************************************************************************/
static RESULT SC031GS_IsiChangeSensorResolutionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            Resolution,
    uint8_t             *pNumberOfFramesToSkip
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if (pNumberOfFramesToSkip == NULL)
    {
        return ( RET_NULL_POINTER );
    }

    if ( (pSensorCtx->Configured != BOOL_TRUE) )
    {
        return RET_WRONG_STATE;
    }

    IsiSensorCaps_t Caps;
    Caps.Index = 0;
    Caps.Resolution = 0;
    while (SC031GS_IsiGetCapsIss( handle, &Caps) == RET_SUCCESS) {
        if (Resolution == Caps.Resolution) {            
            break;
        }
        Caps.Index++;
    }

     if (Resolution != Caps.Resolution) {
        return RET_OUTOFRANGE;
    }
	//TRACE( SC031GS_ERROR, "%s (11111111enter)  \n", __FUNCTION__);
    if ( Resolution == pSensorCtx->Config.Resolution )
    {
        // well, no need to worry
        *pNumberOfFramesToSkip = 0;
    }
    else
    {
        // change resolution
        char *szResName = NULL;

		bool_t res_no_chg;
		//TRACE( SC031GS_ERROR, "%s (2222222222enter)  \n", __FUNCTION__);
        if (!((ISI_RES_W_GET(Resolution)==ISI_RES_W_GET(pSensorCtx->Config.Resolution)) && 
            (ISI_RES_W_GET(Resolution)==ISI_RES_W_GET(pSensorCtx->Config.Resolution))) ) {

            if (pSensorCtx->Streaming != BOOL_FALSE) {
                TRACE( SC031GS_ERROR, "%s: Sensor is streaming, Change resolution is not allow\n",__FUNCTION__);
                return RET_WRONG_STATE;
            }
            res_no_chg = BOOL_FALSE;
        } else {
            res_no_chg = BOOL_TRUE;
        }
		//TRACE( SC031GS_ERROR, "%s(%d) enter....  \n", __FUNCTION__, __LINE__);
        result = IsiGetResolutionName( Resolution, &szResName );
        TRACE( SC031GS_INFO, "%s: NewRes=0x%08x (%s)\n", __FUNCTION__, Resolution, szResName);

        // update resolution in copy of config in context
        pSensorCtx->Config.Resolution = Resolution;

        // tell sensor about that
        result = SC031GS_SetupOutputWindowInternal( pSensorCtx, &pSensorCtx->Config, BOOL_TRUE, res_no_chg );
        if ( result != RET_SUCCESS )
        {
            TRACE( SC031GS_ERROR, "%s: SetupOutputWindow failed.\n", __FUNCTION__);
            return ( result );
        }

        // remember old exposure values
        float OldGain = pSensorCtx->AecCurGain;
        float OldIntegrationTime = pSensorCtx->AecCurIntegrationTime;

        // update limits & stuff (reset current & old settings)
        result = SC031GS_AecSetModeParameters( pSensorCtx, &pSensorCtx->Config );
        if ( result != RET_SUCCESS )
        {
            TRACE( SC031GS_ERROR, "%s: AecSetModeParameters failed.\n", __FUNCTION__);
            return ( result );
        }

        // restore old exposure values (at least within new exposure values' limits)
        uint8_t NumberOfFramesToSkip;
        float   DummySetGain;
        float   DummySetIntegrationTime;
        result = SC031GS_IsiExposureControlIss( handle, OldGain, OldIntegrationTime, &NumberOfFramesToSkip, &DummySetGain, &DummySetIntegrationTime );
        if ( result != RET_SUCCESS )
        {
            TRACE( SC031GS_ERROR, "%s: SC031GS_IsiExposureControlIss failed.\n", __FUNCTION__);
            return ( result );
        }

        // return number of frames that aren't exposed correctly
        *pNumberOfFramesToSkip = NumberOfFramesToSkip + 1;
        //	*pNumberOfFramesToSkip = 0;
    }

    TRACE( SC031GS_INFO, "%s (exit)  result: 0x%x\n", __FUNCTION__, result);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiSensorSetStreamingIss
 *
 * @brief   Enables/disables streaming of sensor data, if possible.
 *
 * @param   handle      Sensor instance handle
 * @param   on          new streaming state (BOOL_TRUE=on, BOOL_FALSE=off)
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_WRONG_STATE
 *
 *****************************************************************************/
static RESULT SC031GS_IsiSensorSetStreamingIss
(
    IsiSensorHandle_t   handle,
    bool_t              on
)
{
    uint32_t RegValue = 0;
	uint32_t RegValue2 = 0;

    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)  on = %d\n", __FUNCTION__,on);
    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( (pSensorCtx->Configured != BOOL_TRUE) || (pSensorCtx->Streaming == on) )
    {
        return RET_WRONG_STATE;
    }

    if (on == BOOL_TRUE)
    {
        /* enable streaming */
		result = SC031GS_IsiRegReadIss ( pSensorCtx, SC031GS_MODE_SELECT, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, SC031GS_MODE_SELECT, (RegValue | 0x01U) );
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		/* color bar */
		#if 0
		result = SC031GS_IsiRegReadIss ( pSensorCtx, 0x4501, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, 0x4501, (RegValue | 0x08U));
		result = SC031GS_IsiRegReadIss ( pSensorCtx, 0x3e06, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, 0x3e06, (RegValue | 0x01U));
		#endif
		TRACE(SC031GS_INFO," STREAM ON ++++++++++++++");
		//while(1);
    }
    else
    {   
        /* disable streaming */
		result = SC031GS_IsiRegReadIss ( pSensorCtx, SC031GS_MODE_SELECT, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, SC031GS_MODE_SELECT, (RegValue & ~0x01U) );
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		/* color bar */
		#if 0
		result = SC031GS_IsiRegReadIss ( pSensorCtx, 0x4501, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, 0x4501, (RegValue & 0xf7U));
		result = SC031GS_IsiRegReadIss ( pSensorCtx, 0x3e06, &RegValue);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss ( pSensorCtx, 0x3e06, (RegValue | 0x01U));
		#endif
        TRACE(SC031GS_INFO," STREAM OFF ++++++++++++++");
    }

    if (result == RET_SUCCESS)
    {
        pSensorCtx->Streaming = on;
    }

    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiSensorSetPowerIss
 *
 * @brief   Performs the power-up/power-down sequence of the camera, if possible.
 *
 * @param   handle      Sensor instance handle
 * @param   on          new power state (BOOL_TRUE=on, BOOL_FALSE=off)
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiSensorSetPowerIss
(
    IsiSensorHandle_t   handle,
    bool_t              on
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    pSensorCtx->Configured = BOOL_FALSE;
    pSensorCtx->Streaming  = BOOL_FALSE;

    result = HalSetPower( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, false );
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    result = HalSetReset( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, true );
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    if (on == BOOL_TRUE)
    {
        result = HalSetPower( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, true );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

        osSleep( 10 );

        result = HalSetReset( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, false );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

        osSleep( 10 );

        result = HalSetReset( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, true );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

        osSleep( 10 );

        result = HalSetReset( pSensorCtx->IsiCtx.HalHandle, pSensorCtx->IsiCtx.HalDevID, false );

        osSleep( 50 );
    }

    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiCheckSensorConnectionIss
 *
 * @brief   Checks the I2C-Connection to sensor by reading sensor revision id.
 *
 * @param   handle      Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiCheckSensorConnectionIss
(
    IsiSensorHandle_t   handle
)
{
    uint32_t RevId;
    uint32_t value;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    RevId = SC031GS_CHIP_ID_HIGH_BYTE_DEFAULT;
    RevId = (RevId << 8U) | (SC031GS_CHIP_ID_LOW_BYTE_DEFAULT);

    result = SC031GS_IsiGetSensorRevisionIss( handle, &value );

    if ( (result != RET_SUCCESS) || (RevId != value) )
    {
        TRACE( SC031GS_INFO, "%s RevId = 0x%08x, value = 0x%08x \n", __FUNCTION__, RevId, value );
        return ( RET_FAILURE );
    }


    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetSensorRevisionIss
 *
 * @brief   reads the sensor revision register and returns this value
 *
 * @param   handle      pointer to sensor description struct
 * @param   p_value     pointer to storage value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetSensorRevisionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            *p_value
)
{
    RESULT result = RET_SUCCESS;

    uint32_t data;
	uint32_t vcm_pos = MAX_LOG;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( p_value == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *p_value = 0U;
    result = SC031GS_IsiRegReadIss ( handle, SC031GS_CHIP_ID_HIGH_BYTE, &data );
    *p_value = ( (data & 0xFF) << 8U );
    result = SC031GS_IsiRegReadIss ( handle, SC031GS_CHIP_ID_LOW_BYTE, &data );
    *p_value |= ( (data & 0xFF) );
	TRACE( SC031GS_INFO, "%s(%d) Check ID reg: 0x%02x, 0x%02x, val: 0x%04x;default:0x%02x, 0x%02x", 
	__FUNCTION__, __LINE__,SC031GS_CHIP_ID_HIGH_BYTE,SC031GS_CHIP_ID_LOW_BYTE,*p_value,
	SC031GS_CHIP_ID_HIGH_BYTE_DEFAULT, SC031GS_CHIP_ID_LOW_BYTE_DEFAULT);
    TRACE( SC031GS_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiRegReadIss
 *
 * @brief   grants user read access to the camera register
 *
 * @param   handle      pointer to sensor description struct
 * @param   address     sensor register to write
 * @param   p_value     pointer to value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiRegReadIss
(
    IsiSensorHandle_t   handle,
    const uint32_t      address,
    uint32_t            *p_value
)
{
	IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;
    RESULT result = RET_SUCCESS;
	uint8_t NrOfBytes;

	TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( p_value == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {
    	NrOfBytes = IsiGetNrDatBytesIss( address, SC031GS_g_aRegDescription);
    	if ( !NrOfBytes )
    	{
        	NrOfBytes = 1;
    	}
        *p_value = 0;
        result = IsiI2cReadSensorRegister( handle, address, (uint8_t *)p_value, 1, BOOL_TRUE );
    }

	TRACE( SC031GS_INFO, "%s (exit: reg: 0x%x reg_value: 0x%02x)\n", __FUNCTION__, address, *p_value);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiRegWriteIss
 *
 * @brief   grants user write access to the camera register
 *
 * @param   handle      pointer to sensor description struct
 * @param   address     sensor register to write
 * @param   value       value to write
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 *
 *****************************************************************************/
static RESULT SC031GS_IsiRegWriteIss
(
    IsiSensorHandle_t   handle,
    const uint32_t      address,
    const uint32_t      value
)
{
    RESULT result = RET_SUCCESS;

    uint8_t NrOfBytes;

  //  TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    NrOfBytes = IsiGetNrDatBytesIss( address, SC031GS_g_aRegDescription);
    if ( !NrOfBytes )
    {
        NrOfBytes = 1;
    }
//    TRACE( SC031GS_REG_DEBUG, "%s (IsiGetNrDatBytesIss %d 0x%08x 0x%08x)\n", __FUNCTION__, NrOfBytes, address, value);

    result = IsiI2cWriteSensorRegister( handle, address, (uint8_t *)(&value), NrOfBytes, BOOL_TRUE );

//    TRACE( SC031GS_ERROR, "%s (exit: 0x%08x 0x%08x)\n", __FUNCTION__, address, value);

    return ( result );
}

#define SC031GS_ANALOG_GAIN_1 64    /*1.00x*/
#define SC031GS_ANALOG_GAIN_2 88    /*1.375x*/
#define SC031GS_ANALOG_GAIN_3 122   /*1.90x*/
#define SC031GS_ANALOG_GAIN_4 168   /*2.625x*/
#define SC031GS_ANALOG_GAIN_5 239   /*3.738x*/
#define SC031GS_ANALOG_GAIN_6 330   /*5.163x*/
#define SC031GS_ANALOG_GAIN_7 470   /*7.350x*/

#define SC031GS_ANALOG_GAIN_REG          0xb6/* Bit 8 */
#define SC031GS_PREGAIN_HIGHBITS_REG     0xb1
#define SC031GS_PREGAIN_LOWBITS_REG      0xb2


#define SC031GS_AEC_EXPO_COARSE_2ND_REG      0x03    /* Exposure Bits 8-15 */
#define SC031GS_AEC_EXPO_COARSE_1ST_REG      0x04    /* Exposure Bits 0-7 */

#define SC031GS_FETCH_2ND_BYTE_EXP(VAL)      ((VAL >> 8) & 0x3F) /* 4 Bits */
#define SC031GS_FETCH_1ST_BYTE_EXP(VAL)      (VAL & 0xFF)	/* 8 Bits */

/*****************************************************************************/
/**
 *          SC031GS_IsiGetGainLimitsIss
 *
 * @brief   Returns the exposure minimal and maximal values of an
 *          Sensor instance
 *
 * @param   handle       Sensor instance handle
 * @param   pMinExposure Pointer to a variable receiving minimal exposure value
 * @param   pMaxExposure Pointer to a variable receiving maximal exposure value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetGainLimitsIss
(
    IsiSensorHandle_t   handle,
    float               *pMinGain,
    float               *pMaxGain
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;


    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( (pMinGain == NULL) || (pMaxGain == NULL) )
    {
        TRACE( SC031GS_ERROR, "%s: NULL pointer received!!\n" );
        return ( RET_NULL_POINTER );
    }

    *pMinGain = pSensorCtx->AecMinGain;
    *pMaxGain = pSensorCtx->AecMaxGain;

    TRACE( SC031GS_INFO, "%s: pMinGain:%f,pMaxGain:%f(exit)\n", __FUNCTION__,pSensorCtx->AecMinGain,pSensorCtx->AecMaxGain);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetIntegrationTimeLimitsIss
 *
 * @brief   Returns the minimal and maximal integration time values of an
 *          Sensor instance
 *
 * @param   handle       Sensor instance handle
 * @param   pMinExposure Pointer to a variable receiving minimal exposure value
 * @param   pMaxExposure Pointer to a variable receiving maximal exposure value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetIntegrationTimeLimitsIss
(
    IsiSensorHandle_t   handle,
    float               *pMinIntegrationTime,
    float               *pMaxIntegrationTime
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;


    TRACE( SC031GS_INFO, "%s: (------oyyf enter) \n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( (pMinIntegrationTime == NULL) || (pMaxIntegrationTime == NULL) )
    {
        TRACE( SC031GS_ERROR, "%s: NULL pointer received!!\n" );
        return ( RET_NULL_POINTER );
    }

    *pMinIntegrationTime = pSensorCtx->AecMinIntegrationTime;
    *pMaxIntegrationTime = pSensorCtx->AecMaxIntegrationTime;
	TRACE( SC031GS_INFO, "%s(%d): AecMinIntegrationTime: %f, AecMaxIntegrationTime:%f\n", 
		__FUNCTION__, __LINE__, pSensorCtx->AecMinIntegrationTime, pSensorCtx->AecMaxIntegrationTime);
    TRACE( SC031GS_INFO, "%s: (------oyyf exit) (\n", __FUNCTION__);

    return ( result );
}


/*****************************************************************************/
/**
 *          SC031GS_IsiGetGainIss
 *
 * @brief   Reads gain values from the image sensor module.
 *
 * @param   handle                  Sensor instance handle
 * @param   pSetGain                set gain
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetGainIss
(
    IsiSensorHandle_t   handle,
    float               *pSetGain
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pSetGain == NULL)
    {
        return ( RET_NULL_POINTER );
    }

    *pSetGain = pSensorCtx->AecCurGain;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetGainIncrementIss
 *
 * @brief   Get smallest possible gain increment.
 *
 * @param   handle                  Sensor instance handle
 * @param   pIncr                   increment
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetGainIncrementIss
(
    IsiSensorHandle_t   handle,
    float               *pIncr
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pIncr == NULL)
    {
        return ( RET_NULL_POINTER );
    }

    //_smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application)
    *pIncr = pSensorCtx->AecGainIncrement;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiSetGainIss
 *
 * @brief   Writes gain values to the image sensor module.
 *          Updates current gain and exposure in sensor struct/state.
 *
 * @param   handle                  Sensor instance handle
 * @param   NewGain                 gain to be set
 * @param   pSetGain                set gain
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 *
 *****************************************************************************/
RESULT SC031GS_IsiSetGainIss
(
    IsiSensorHandle_t   handle,
    float               NewGain,
    float               *pSetGain,
    uint8_t *pNumberOfFramesToSkip
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
    RESULT result = RET_SUCCESS;

	uint16_t usGain = 0;
    uint8_t temp = 0;
	TRACE( SC031GS_INFO, "%s(%d): (enter)\n", __FUNCTION__, __LINE__);
    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pSetGain == NULL)
    {
        TRACE( SC031GS_ERROR, "%s: Invalid parameter (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_NULL_POINTER );
    }
  
    if( NewGain < pSensorCtx->AecMinGain ) NewGain = pSensorCtx->AecMinGain;
    if( NewGain > pSensorCtx->AecMaxGain ) NewGain = pSensorCtx->AecMaxGain;
	
	usGain = (uint16_t)(NewGain * 16.0f+ 0.5f);
	
    // write new gain into sensor registers, do not write if nothing has changed
    if( (usGain != pSensorCtx->OldGain) )
    {
		result = SC031GS_IsiRegWriteIss(pSensorCtx, 0x3e08, (usGain>>8)&0x03);
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss(pSensorCtx, 0x3e09, (usGain&0xff));
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		*pNumberOfFramesToSkip = 2U; //skip 2 frames
    }
    else{
        *pNumberOfFramesToSkip = 0U; //skip 0 frame
    }

    pSensorCtx->OldGain = usGain;
    //calculate gain actually set
    pSensorCtx->AecCurGain = ( (float)usGain ) / 16.0f;

    //return current state
    *pSetGain = pSensorCtx->AecCurGain;
    TRACE( SC031GS_INFO, "%s: setgain mubiao(%f) shiji(%f) usGain(%d)\n", __FUNCTION__, NewGain, *pSetGain,usGain);
	TRACE( SC031GS_INFO, "%s(%d): (exit)\n", __FUNCTION__, __LINE__);
    return ( result );
}

     

/*****************************************************************************/
/**
 *          SC031GS_IsiGetIntegrationTimeIss
 *
 * @brief   Reads integration time values from the image sensor module.
 *
 * @param   handle                  Sensor instance handle
 * @param   pSetIntegrationTime     set integration time
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetIntegrationTimeIss
(
    IsiSensorHandle_t   handle,
    float               *pSetIntegrationTime
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pSetIntegrationTime == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pSetIntegrationTime = pSensorCtx->AecCurIntegrationTime;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetIntegrationTimeIncrementIss
 *
 * @brief   Get smallest possible integration time increment.
 *
 * @param   handle                  Sensor instance handle
 * @param   pIncr                   increment
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetIntegrationTimeIncrementIss
(
    IsiSensorHandle_t   handle,
    float               *pIncr
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (-------oyyf)(enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pIncr == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //_smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application)
    *pIncr = (float)pSensorCtx->AecIntegrationTimeIncrement;

    TRACE( SC031GS_INFO, "%s: (------oyyf)(exit) pSensorCtx->AecIntegrationTimeIncrement(%u)\n", __FUNCTION__,pSensorCtx->AecIntegrationTimeIncrement);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiSetIntegrationTimeIss
 *
 * @brief   Writes gain and integration time values to the image sensor module.
 *          Updates current integration time and exposure in sensor
 *          struct/state.
 *
 * @param   handle                  Sensor instance handle
 * @param   NewIntegrationTime      integration time to be set
 * @param   pSetIntegrationTime     set integration time
 * @param   pNumberOfFramesToSkip   number of frames to skip until AE is
 *                                  executed again
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 * @retval  RET_DIVISION_BY_ZERO
 *
 *****************************************************************************/
RESULT SC031GS_IsiSetIntegrationTimeIss
(
    IsiSensorHandle_t   handle,
    float               NewIntegrationTime,
    float               *pSetIntegrationTime,
    uint8_t 			*pNumberOfFramesToSkip
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
    RESULT result = RET_SUCCESS;

	uint32_t CoarseIntegrationTime = 0;
	//uint32_t FineIntegrationTime   = 0; //not supported by Sensor
	uint32_t result_intertime= 0;
	float ShutterWidthPck = 0.0f; //shutter width in pixel clock periods

    TRACE( SC031GS_INFO, "%s: (enter) NewIntegrationTime: %f (min: %lu   max: %lu)\n", __FUNCTION__,
        NewIntegrationTime,
        pSensorCtx->AecMinIntegrationTime,
        pSensorCtx->AecMaxIntegrationTime);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( (pSetIntegrationTime == NULL) || (pNumberOfFramesToSkip == NULL) )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid parameter (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_NULL_POINTER );
    }

    //maximum and minimum integration time is limited by the sensor, if this limit is not
    //considered, the exposure control loop needs lots of time to return to a new state
    //so limit to allowed range
    if ( NewIntegrationTime > pSensorCtx->AecMaxIntegrationTime ) NewIntegrationTime = pSensorCtx->AecMaxIntegrationTime;
    if ( NewIntegrationTime < pSensorCtx->AecMinIntegrationTime ) NewIntegrationTime = pSensorCtx->AecMinIntegrationTime;
	ShutterWidthPck = NewIntegrationTime * ( (float)pSensorCtx->VtPixClkFreq );
	// avoid division by zero
    if ( pSensorCtx->LineLengthPck == 0 )
    {
        TRACE( SC031GS_ERROR, "%s: Division by zero!\n", __FUNCTION__ );
        return ( RET_DIVISION_BY_ZERO );
    }
	CoarseIntegrationTime = (uint32_t)( ShutterWidthPck / ((float)pSensorCtx->LineLengthPck) + 0.5f ); //1/16
	#if 1
	// write new integration time into sensor registers
	// do not write if nothing has changed
	if( CoarseIntegrationTime != pSensorCtx->OldCoarseIntegrationTime )
	{
		result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x3e01, (CoarseIntegrationTime & 0x00000FF0U) >> 4U );
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
		result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x3e02, (CoarseIntegrationTime & 0x0000000FU) << 4U );
		RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
	
		pSensorCtx->OldCoarseIntegrationTime = CoarseIntegrationTime;	// remember current integration time
		*pNumberOfFramesToSkip = 2U; //skip 1 frame
	}
	else
	{
		*pNumberOfFramesToSkip = 0U; //no frame skip
	}
	#endif
	//calculate integration time actually set
    //pSensorCtx->AecCurIntegrationTime = ( ((float)CoarseIntegrationTime) * ((float)pSensorCtx->LineLengthPck) + ((float)FineIntegrationTime) ) / pSensorCtx->VtPixClkFreq;
    pSensorCtx->AecCurIntegrationTime = ((float)CoarseIntegrationTime) * ((float)pSensorCtx->LineLengthPck) / pSensorCtx->VtPixClkFreq;

    //return current state
    *pSetIntegrationTime = pSensorCtx->AecCurIntegrationTime;

   // TRACE( Sensor_ERROR, "%s: SetTi=%f NewTi=%f\n", __FUNCTION__, *pSetIntegrationTime,NewIntegrationTime);
    TRACE( SC031GS_INFO, "%s(%d): settime mubiao(%f) shiji(%f)\n", __FUNCTION__, __LINE__, NewIntegrationTime,*pSetIntegrationTime);
	TRACE( SC031GS_DEBUG, "%s(%d):ethan "
         "pSensorCtx->VtPixClkFreq:%f pSensorCtx->LineLengthPck:%x ,"
         "*pSetIntegrationTime=%f    NewIntegrationTime=%f  CoarseIntegrationTime=%x ,"
         "result_intertime = %x\n H:%x\n L:%x", __FUNCTION__, __LINE__,
         pSensorCtx->VtPixClkFreq,pSensorCtx->LineLengthPck,
         *pSetIntegrationTime,NewIntegrationTime,CoarseIntegrationTime,
         result_intertime,
         (CoarseIntegrationTime & 0x00000FF0U) >> 4U,
         (CoarseIntegrationTime & 0x0000000FU) << 4U);
    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiExposureControlIss
 *
 * @brief   Camera hardware dependent part of the exposure control loop.
 *          Calculates appropriate register settings from the new exposure
 *          values and writes them to the image sensor module.
 *
 * @param   handle                  Sensor instance handle
 * @param   NewGain                 newly calculated gain to be set
 * @param   NewIntegrationTime      newly calculated integration time to be set
 * @param   pNumberOfFramesToSkip   number of frames to skip until AE is
 *                                  executed again
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 * @retval  RET_DIVISION_BY_ZERO
 *
 *****************************************************************************/
RESULT SC031GS_IsiExposureControlIss
(
    IsiSensorHandle_t   handle,
    float               NewGain,
    float               NewIntegrationTime,
    uint8_t             *pNumberOfFramesToSkip,
    float               *pSetGain,
    float               *pSetIntegrationTime
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s(%d): (enter)\n", __FUNCTION__, __LINE__);
    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( (pNumberOfFramesToSkip == NULL)
            || (pSetGain == NULL)
            || (pSetIntegrationTime == NULL) )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid parameter (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_NULL_POINTER );
    }

    TRACE( SC031GS_DEBUG, "%s: g=%f, Ti=%f\n", __FUNCTION__, NewGain, NewIntegrationTime );

	//Group hold
	result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x3812, 0x00);
	RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    result = SC031GS_IsiSetIntegrationTimeIss( handle, NewIntegrationTime, pSetIntegrationTime, pNumberOfFramesToSkip);
    result = SC031GS_IsiSetGainIss( handle, NewGain, pSetGain, pNumberOfFramesToSkip);
	
	result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x3812, 0x30);
	RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
 
    TRACE( SC031GS_INFO, "%s: set: g=%f, Ti=%f, skip=%d\n", __FUNCTION__, *pSetGain, *pSetIntegrationTime, *pNumberOfFramesToSkip ); 
	TRACE( SC031GS_INFO, "%s(%d) exit...\n", __FUNCTION__, __LINE__); 
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCurrentExposureIss
 *
 * @brief   Returns the currently adjusted AE values
 *
 * @param   handle                  Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetCurrentExposureIss
(
    IsiSensorHandle_t   handle,
    float               *pSetGain,
    float               *pSetIntegrationTime
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( (pSetGain == NULL) || (pSetIntegrationTime == NULL) )
    {
        return ( RET_NULL_POINTER );
    }

    *pSetGain            = pSensorCtx->AecCurGain;
    *pSetIntegrationTime = pSensorCtx->AecCurIntegrationTime * 1.0;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetResolutionIss
 *
 * @brief   Reads integration time values from the image sensor module.
 *
 * @param   handle                  sensor instance handle
 * @param   pSettResolution         set resolution
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetResolutionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            *pSetResolution
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pSetResolution == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pSetResolution = pSensorCtx->Config.Resolution;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetAfpsInfoHelperIss
 *
 * @brief   Calc AFPS sub resolution settings for the given resolution
 *
 * @param   pSensorCtx             Sensor instance (dummy!) context
 * @param   Resolution              Any supported resolution to query AFPS params for
 * @param   pAfpsInfo               Reference of AFPS info structure to write the results to
 * @param   AfpsStageIdx            Index of current AFPS stage to use
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetAfpsInfoHelperIss(
    SC031GS_Context_t   *pSensorCtx,
    uint32_t            Resolution,
    IsiAfpsInfo_t*      pAfpsInfo,
    uint32_t            AfpsStageIdx
)
{
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (-----------ethan enter) pAfpsInfo->AecMaxIntTime(%lu) pSensorCtx->AecMaxIntegrationTime(%lu)\n", __FUNCTION__, pAfpsInfo->AecMaxIntTime,pSensorCtx->AecMaxIntegrationTime);

    DCT_ASSERT(pSensorCtx != NULL);
    DCT_ASSERT(pAfpsInfo != NULL);
    DCT_ASSERT(AfpsStageIdx <= ISI_NUM_AFPS_STAGES);

    // update resolution in copy of config in context
    pSensorCtx->Config.Resolution = Resolution;

    // tell sensor about that
    result = SC031GS_SetupOutputWindowInternal( pSensorCtx, &pSensorCtx->Config,BOOL_FALSE,BOOL_FALSE);
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: SetupOutputWindow failed for resolution ID %08x.\n", __FUNCTION__, Resolution);
        return ( result );
    }

    // update limits & stuff (reset current & old settings)
    result = SC031GS_AecSetModeParameters( pSensorCtx, &pSensorCtx->Config );
    if ( result != RET_SUCCESS )
    {
        TRACE( SC031GS_ERROR, "%s: AecSetModeParameters failed for resolution ID %08x.\n", __FUNCTION__, Resolution);
        return ( result );
    }

    // take over params
    pAfpsInfo->Stage[AfpsStageIdx].Resolution = Resolution;
    pAfpsInfo->Stage[AfpsStageIdx].MaxIntTime = pSensorCtx->AecMaxIntegrationTime * 1.0;
    pAfpsInfo->AecMinGain           = pSensorCtx->AecMinGain;
    pAfpsInfo->AecMaxGain           = pSensorCtx->AecMaxGain;
    pAfpsInfo->AecMinIntTime        = pSensorCtx->AecMinIntegrationTime * 1.0;
    pAfpsInfo->AecMaxIntTime        = pSensorCtx->AecMaxIntegrationTime * 1.0;
    pAfpsInfo->AecSlowestResolution = Resolution;

    TRACE( SC031GS_DEBUG, "%s: (-----------ethan exit) pAfpsInfo->AecMaxIntTime(%lu) pSensorCtx->AecMaxIntegrationTime(%lu)\n", __FUNCTION__, pAfpsInfo->AecMaxIntTime,pSensorCtx->AecMaxIntegrationTime);

    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiGetAfpsInfoIss
 *
 * @brief   Returns the possible AFPS sub resolution settings for the given resolution series
 *
 * @param   handle                  Sensor instance handle
 * @param   Resolution              Any resolution within the AFPS group to query;
 *                                  0 (zero) to use the currently configured resolution
 * @param   pAfpsInfo               Reference of AFPS info structure to store the results
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_NOTSUPP
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetAfpsInfoIss(
    IsiSensorHandle_t   handle,
    uint32_t            Resolution,
    IsiAfpsInfo_t*      pAfpsInfo
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    uint32_t RegValue = 0;

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pAfpsInfo == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    // use currently configured resolution?
    if (Resolution == 0)
    {
        Resolution = pSensorCtx->Config.Resolution;
    }

    // prepare index
    uint32_t idx = 0;

    // set current resolution data in info struct
    pAfpsInfo->CurrResolution = pSensorCtx->Config.Resolution;
    pAfpsInfo->CurrMinIntTime = pSensorCtx->AecMinIntegrationTime;
    pAfpsInfo->CurrMaxIntTime = pSensorCtx->AecMaxIntegrationTime;

    // allocate dummy context used for Afps parameter calculation as a copy of current context
    SC031GS_Context_t *pDummyCtx = (SC031GS_Context_t*) malloc( sizeof(SC031GS_Context_t) );
    if ( pDummyCtx == NULL )
    {
        TRACE( SC031GS_ERROR,  "%s: Can't allocate dummy gc2355 context\n",  __FUNCTION__ );
        return ( RET_OUTOFMEM );
    }
    *pDummyCtx = *pSensorCtx;

    // set AFPS mode in dummy context
    pDummyCtx->isAfpsRun = BOOL_TRUE;

#define AFPSCHECKANDADD(_res_) \
    { \
        RESULT lres = SC031GS_IsiGetAfpsInfoHelperIss( pDummyCtx, _res_, pAfpsInfo, idx ); \
        if ( lres == RET_SUCCESS ) \
        { \
            ++idx; \
        } \
        else \
        { \
            UPDATE_RESULT( result, lres ); \
        } \
    }

    // check which AFPS series is requested and build its params list for the enabled AFPS resolutions
	switch(Resolution)
	{
        default:
			TRACE( SC031GS_ERROR,  "%s: Resolution %08x not supported by AFPS\n",  __FUNCTION__, Resolution );
            result = RET_NOTSUPP;
            break;
		case ISI_RES_VGAP30:
		case ISI_RES_VGAP20:
		case ISI_RES_VGAP15:
		case ISI_RES_VGAP10:
			TRACE( SC031GS_ERROR, "%s: preview_minimum_framerate(%d)\n", __FUNCTION__,pSensorCtx->preview_minimum_framerate);
			if(ISI_FPS_GET(ISI_RES_VGAP30) >= pSensorCtx->preview_minimum_framerate)
				AFPSCHECKANDADD( ISI_RES_VGAP30);
			if(ISI_FPS_GET(ISI_RES_VGAP20) >= pSensorCtx->preview_minimum_framerate)
				AFPSCHECKANDADD( ISI_RES_VGAP20);
			if(ISI_FPS_GET(ISI_RES_VGAP15) >= pSensorCtx->preview_minimum_framerate)
				AFPSCHECKANDADD( ISI_RES_VGAP15);
			if(ISI_FPS_GET(ISI_RES_VGAP10) >= pSensorCtx->preview_minimum_framerate)
				AFPSCHECKANDADD( ISI_RES_VGAP10);
			break;
		
		
		/*
		case ISI_RES_SVGAP30:
			//TRACE( SC031GS_ERROR, "%s: (88888exit)\n", __FUNCTION__);
			AFPSCHECKANDADD( ISI_RES_SVGAP30);
			break;
		*/
	}

    // release dummy context again
    free(pDummyCtx);

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}


/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibKFactor
 *
 * @brief   Returns the Sensor specific K-Factor
 *
 * @param   handle       Sensor instance handle
 * @param   pIsiKFactor  Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibKFactor
(
    IsiSensorHandle_t   handle,
    Isi1x1FloatMatrix_t **pIsiKFactor
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiKFactor == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*pIsiKFactor = (Isi1x1FloatMatrix_t *)&SC031GS_KFactor;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}


/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibPcaMatrix
 *
 * @brief   Returns the Sensor specific PCA-Matrix
 *
 * @param   handle          Sensor instance handle
 * @param   pIsiPcaMatrix   Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibPcaMatrix
(
    IsiSensorHandle_t   handle,
    Isi3x2FloatMatrix_t **pIsiPcaMatrix
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiPcaMatrix == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*pIsiPcaMatrix = (Isi3x2FloatMatrix_t *)&SC031GS_PCAMatrix;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibSvdMeanValue
 *
 * @brief   Returns the sensor specific SvdMean-Vector
 *
 * @param   handle              Sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibSvdMeanValue
(
    IsiSensorHandle_t   handle,
    Isi3x1FloatMatrix_t **pIsiSvdMeanValue
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiSvdMeanValue == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*pIsiSvdMeanValue = (Isi3x1FloatMatrix_t *)&SC031GS_SVDMeanValue;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibSvdMeanValue
 *
 * @brief   Returns a pointer to the sensor specific centerline, a straight
 *          line in Hesse normal form in Rg/Bg colorspace
 *
 * @param   handle              Sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibCenterLine
(
    IsiSensorHandle_t   handle,
    IsiLine_t           **ptIsiCenterLine
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiCenterLine == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*ptIsiCenterLine = (IsiLine_t*)&SC031GS_CenterLine;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibClipParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for Rg/Bg color
 *          space clipping
 *
 * @param   handle              Sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibClipParam
(
    IsiSensorHandle_t   handle,
    IsiAwbClipParm_t    **pIsiClipParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiClipParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*pIsiClipParam = (IsiAwbClipParm_t *)&SC031GS_AwbClipParm;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibGlobalFadeParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for AWB out of
 *          range handling
 *
 * @param   handle              Sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibGlobalFadeParam
(
    IsiSensorHandle_t       handle,
    IsiAwbGlobalFadeParm_t  **ptIsiGlobalFadeParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiGlobalFadeParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*ptIsiGlobalFadeParam = (IsiAwbGlobalFadeParm_t *)&SC031GS_AwbGlobalFadeParm;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetCalibFadeParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for near white
 *          pixel parameter calculations
 *
 * @param   handle              Sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetCalibFadeParam
(
    IsiSensorHandle_t   handle,
    IsiAwbFade2Parm_t   **ptIsiFadeParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiFadeParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    //*ptIsiFadeParam = (IsiAwbFade2Parm_t *)&SC031GS_AwbFade2Parm;

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiGetIlluProfile
 *
 * @brief   Returns a pointer to illumination profile idetified by CieProfile
 *          bitmask
 *
 * @param   handle              sensor instance handle
 * @param   CieProfile
 * @param   ptIsiIlluProfile    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetIlluProfile
(
    IsiSensorHandle_t   handle,
    const uint32_t      CieProfile,
    IsiIlluProfile_t    **ptIsiIlluProfile
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiIlluProfile == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetLscMatrixTable
 *
 * @brief   Returns a pointer to illumination profile idetified by CieProfile
 *          bitmask
 *
 * @param   handle              sensor instance handle
 * @param   CieProfile
 * @param   ptIsiIlluProfile    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiGetLscMatrixTable
(
    IsiSensorHandle_t   handle,
    const uint32_t      CieProfile,
    IsiLscMatrixTable_t **pLscMatrixTable
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pLscMatrixTable == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}


/*****************************************************************************/
/**
 *          SC031GS_IsiMdiInitMotoDriveMds
 *
 * @brief   General initialisation tasks like I/O initialisation.
 *
 * @param   handle              Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiMdiInitMotoDriveMds
(
    IsiSensorHandle_t   handle
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    #if 1
    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);
    #endif
    
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiMdiSetupMotoDrive
 *
 * @brief   Setup of the MotoDrive and return possible max step.
 *
 * @param   handle          Sensor instance handle
 *          pMaxStep        pointer to variable to receive the maximum
 *                          possible focus step
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiMdiSetupMotoDrive
(
    IsiSensorHandle_t   handle,
    uint32_t            *pMaxStep
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;
	uint32_t vcm_movefull_t;
    RESULT result = RET_SUCCESS;

    #if 1
    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pMaxStep == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);
    #endif
    
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiMdiFocusSet
 *
 * @brief   Drives the lens system to a certain focus point.
 *
 * @param   handle          Sensor instance handle
 *          AbsStep         absolute focus point to apply
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiMdiFocusSet
(
    IsiSensorHandle_t   handle,
    const uint32_t      Position
)
{
	SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    
    #if 1
    uint32_t nPosition;
    uint8_t  data[2] = { 0, 0 };

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
    	TRACE( SC031GS_ERROR, "%s: pSensorCtx IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);
    #endif
    
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiMdiFocusGet
 *
 * @brief   Retrieves the currently applied focus point.
 *
 * @param   handle          Sensor instance handle
 *          pAbsStep        pointer to a variable to receive the current
 *                          focus point
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiMdiFocusGet
(
    IsiSensorHandle_t   handle,
    uint32_t            *pAbsStep
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;
    uint8_t  data[2] = { 0, 0 };

    #if 1
    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pAbsStep == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    #endif
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiMdiFocusCalibrate
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT SC031GS_IsiMdiFocusCalibrate
(
    IsiSensorHandle_t   handle
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    #if 1
    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);
    #endif
    
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiActivateTestPattern
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 ******************************************************************************/
static RESULT SC031GS_IsiActivateTestPattern
(
    IsiSensorHandle_t   handle,
    const bool_t        enable
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    #if 0
    uint32_t ulRegValue = 0UL;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( BOOL_TRUE == enable )
    {
        /* enable test-pattern */
        result = SC031GS_IsiRegReadIss( pSensorCtx, 0x5e00, &ulRegValue );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

        ulRegValue |= ( 0x80U );

        result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x5e00, ulRegValue );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
    }
    else
    {
        /* disable test-pattern */
        result = SC031GS_IsiRegReadIss( pSensorCtx, 0x5e00, &ulRegValue );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

        ulRegValue &= ~( 0x80 );

        result = SC031GS_IsiRegWriteIss( pSensorCtx, 0x5e00, ulRegValue );
        RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );
    }

     pSensorCtx->TestPattern = enable;
    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    #endif
    return ( result );
}



/*****************************************************************************/
/**
 *          SC031GS_IsiGetSensorMipiInfoIss
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          Sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 ******************************************************************************/
static RESULT SC031GS_IsiGetSensorMipiInfoIss
(
    IsiSensorHandle_t   handle,
    IsiSensorMipiInfo   *ptIsiSensorMipiInfo
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }


    if ( ptIsiSensorMipiInfo == NULL )
    {
        return ( result );
    }


    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

static RESULT SC031GS_IsiGetSensorIsiVersion
(  IsiSensorHandle_t   handle,
   unsigned int*     pVersion
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;


    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
    	TRACE( SC031GS_ERROR, "%s: pSensorCtx IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
    }

	if(pVersion == NULL)
	{
		TRACE( SC031GS_ERROR, "%s: pVersion IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
	}

	*pVersion = CONFIG_ISI_VERSION;
	return result;
}

static RESULT SC031GS_IsiGetSensorTuningXmlVersion
(  IsiSensorHandle_t   handle,
   char**     pTuningXmlVersion
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;


    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
    	TRACE( SC031GS_ERROR, "%s: pSensorCtx IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
    }

	if(pTuningXmlVersion == NULL)
	{
		TRACE( SC031GS_ERROR, "%s: pVersion IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
	}

	*pTuningXmlVersion = SC031GS_NEWEST_TUNING_XML;
	return result;
}

/*****************************************************************************/
/**
 *          SC031GS_IsiGetColorIss
 *
 *****************************************************************************/

RESULT SC031GS_IsiGetColorIss
(
    IsiSensorHandle_t   handle,
    char	*pGetColor
)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        TRACE( SC031GS_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pGetColor == NULL)
    {
        return ( RET_NULL_POINTER );
    }

    *pGetColor = WHITE_BLACK_SENSOR;


    TRACE( SC031GS_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          SC031GS_IsiGetSensorIss
 *
 * @brief   fills in the correct pointers for the sensor description struct
 *
 * @param   param1      pointer to sensor description struct
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT SC031GS_IsiGetSensorIss
(
    IsiSensor_t *pIsiSensor
)
{
    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s(%d): (enter)\n", __FUNCTION__, __LINE__);

    if ( pIsiSensor != NULL )
    {
        pIsiSensor->pszName                             = SC031GS_g_acName;
        pIsiSensor->pRegisterTable                      = SC031GS_g_aRegDescription;
        pIsiSensor->pIsiSensorCaps                      = &SC031GS_g_IsiSensorDefaultConfig;
		pIsiSensor->pIsiGetSensorIsiVer					= SC031GS_IsiGetSensorIsiVersion;//oyyf
		pIsiSensor->pIsiGetSensorTuningXmlVersion		= SC031GS_IsiGetSensorTuningXmlVersion;//oyyf
		pIsiSensor->pIsiCheckOTPInfo                    = NULL;//check_read_otp;//zyc
        pIsiSensor->pIsiCreateSensorIss                 = SC031GS_IsiCreateSensorIss;
        pIsiSensor->pIsiReleaseSensorIss                = SC031GS_IsiReleaseSensorIss;
        pIsiSensor->pIsiGetCapsIss                      = SC031GS_IsiGetCapsIss;
        pIsiSensor->pIsiSetupSensorIss                  = SC031GS_IsiSetupSensorIss;
        pIsiSensor->pIsiChangeSensorResolutionIss       = SC031GS_IsiChangeSensorResolutionIss;
        pIsiSensor->pIsiSensorSetStreamingIss           = SC031GS_IsiSensorSetStreamingIss;
        pIsiSensor->pIsiSensorSetPowerIss               = SC031GS_IsiSensorSetPowerIss;
        pIsiSensor->pIsiCheckSensorConnectionIss        = SC031GS_IsiCheckSensorConnectionIss;
        pIsiSensor->pIsiGetSensorRevisionIss            = SC031GS_IsiGetSensorRevisionIss;
        pIsiSensor->pIsiRegisterReadIss                 = SC031GS_IsiRegReadIss;
        pIsiSensor->pIsiRegisterWriteIss                = SC031GS_IsiRegWriteIss;

        /* AEC functions */
        pIsiSensor->pIsiExposureControlIss              = SC031GS_IsiExposureControlIss;
        pIsiSensor->pIsiGetGainLimitsIss                = SC031GS_IsiGetGainLimitsIss;
        pIsiSensor->pIsiGetIntegrationTimeLimitsIss     = SC031GS_IsiGetIntegrationTimeLimitsIss;
        pIsiSensor->pIsiGetCurrentExposureIss           = SC031GS_IsiGetCurrentExposureIss;
        pIsiSensor->pIsiGetGainIss                      = SC031GS_IsiGetGainIss;
        pIsiSensor->pIsiGetGainIncrementIss             = SC031GS_IsiGetGainIncrementIss;
        pIsiSensor->pIsiSetGainIss                      = SC031GS_IsiSetGainIss;
        pIsiSensor->pIsiGetIntegrationTimeIss           = SC031GS_IsiGetIntegrationTimeIss;
        pIsiSensor->pIsiGetIntegrationTimeIncrementIss  = SC031GS_IsiGetIntegrationTimeIncrementIss;
        pIsiSensor->pIsiSetIntegrationTimeIss           = SC031GS_IsiSetIntegrationTimeIss;
        pIsiSensor->pIsiGetResolutionIss                = SC031GS_IsiGetResolutionIss;
        pIsiSensor->pIsiGetAfpsInfoIss                  = SC031GS_IsiGetAfpsInfoIss;

        /* AWB specific functions */
        pIsiSensor->pIsiGetCalibKFactor                 = SC031GS_IsiGetCalibKFactor;
        pIsiSensor->pIsiGetCalibPcaMatrix               = SC031GS_IsiGetCalibPcaMatrix;
        pIsiSensor->pIsiGetCalibSvdMeanValue            = SC031GS_IsiGetCalibSvdMeanValue;
        pIsiSensor->pIsiGetCalibCenterLine              = SC031GS_IsiGetCalibCenterLine;
        pIsiSensor->pIsiGetCalibClipParam               = SC031GS_IsiGetCalibClipParam;
        pIsiSensor->pIsiGetCalibGlobalFadeParam         = SC031GS_IsiGetCalibGlobalFadeParam;
        pIsiSensor->pIsiGetCalibFadeParam               = SC031GS_IsiGetCalibFadeParam;
        pIsiSensor->pIsiGetIlluProfile                  = SC031GS_IsiGetIlluProfile;
        pIsiSensor->pIsiGetLscMatrixTable               = SC031GS_IsiGetLscMatrixTable;

        /* AF functions */
        pIsiSensor->pIsiMdiInitMotoDriveMds             = NULL; //SC031GS_IsiMdiInitMotoDriveMds;
        pIsiSensor->pIsiMdiSetupMotoDrive               = NULL; //SC031GS_IsiMdiSetupMotoDrive;
        pIsiSensor->pIsiMdiFocusSet                     = NULL; //SC031GS_IsiMdiFocusSet;
        pIsiSensor->pIsiMdiFocusGet                     = NULL; //SC031GS_IsiMdiFocusGet;
        pIsiSensor->pIsiMdiFocusCalibrate               = NULL; //SC031GS_IsiMdiFocusCalibrate;

        /* MIPI */
        pIsiSensor->pIsiGetSensorMipiInfoIss            = SC031GS_IsiGetSensorMipiInfoIss;

        /* Testpattern */
        pIsiSensor->pIsiActivateTestPattern             = SC031GS_IsiActivateTestPattern;
		pIsiSensor->pIsiGetColorIss               		= SC031GS_IsiGetColorIss;
    }
    else
    {
        result = RET_NULL_POINTER;
    }

    TRACE( SC031GS_INFO, "%s(%d) (exit)\n", __FUNCTION__, __LINE__);

    return ( result );
}

static RESULT SC031GS_IsiGetSensorI2cInfo(sensor_i2c_info_t** pdata)
{
    sensor_i2c_info_t* pSensorI2cInfo;
	TRACE( SC031GS_INFO, "%s(%d): (enter)\n", __FUNCTION__, __LINE__);
    pSensorI2cInfo = ( sensor_i2c_info_t * )malloc ( sizeof (sensor_i2c_info_t) );

    if ( pSensorI2cInfo == NULL )
    {
        TRACE( SC031GS_ERROR,  "%s: Can't allocate ov14825 context\n",  __FUNCTION__ );
        return ( RET_OUTOFMEM );
    }
    MEMSET( pSensorI2cInfo, 0, sizeof( sensor_i2c_info_t ) );

    
    pSensorI2cInfo->i2c_addr = SC031GS_SLAVE_ADDR;
    pSensorI2cInfo->i2c_addr2 = SC031GS_SLAVE_ADDR2;
    pSensorI2cInfo->soft_reg_addr = SC031GS_SOFTWARE_RST;
    pSensorI2cInfo->soft_reg_value = 0x01;
    pSensorI2cInfo->reg_size = 2;
    pSensorI2cInfo->value_size = 1;

    {
        IsiSensorCaps_t Caps;
        sensor_caps_t *pCaps;
        uint32_t lanes,i;        

        for (i=0; i<3; i++) {
            ListInit(&pSensorI2cInfo->lane_res[i]);
        }

        Caps.Index = 0;            
        while(SC031GS_IsiGetCapsIssInternal(&Caps)==RET_SUCCESS) {
            pCaps = malloc(sizeof(sensor_caps_t));
            if (pCaps != NULL) {
                memcpy(&pCaps->caps,&Caps,sizeof(IsiSensorCaps_t));
                ListPrepareItem(pCaps);
                ListAddTail(&pSensorI2cInfo->lane_res[0], pCaps);
            }
            Caps.Index++;
        }

    }
    
    ListInit(&pSensorI2cInfo->chipid_info);

    sensor_chipid_info_t* pChipIDInfo_H = (sensor_chipid_info_t *) malloc( sizeof(sensor_chipid_info_t) );
    if ( !pChipIDInfo_H )
    {
        return RET_OUTOFMEM;
    }
    MEMSET( pChipIDInfo_H, 0, sizeof(*pChipIDInfo_H) );    
    pChipIDInfo_H->chipid_reg_addr = SC031GS_CHIP_ID_HIGH_BYTE;  
    pChipIDInfo_H->chipid_reg_value = SC031GS_CHIP_ID_HIGH_BYTE_DEFAULT;
    ListPrepareItem( pChipIDInfo_H );
    ListAddTail( &pSensorI2cInfo->chipid_info, pChipIDInfo_H );

    sensor_chipid_info_t* pChipIDInfo_L = (sensor_chipid_info_t *) malloc( sizeof(sensor_chipid_info_t) );
    if ( !pChipIDInfo_L )
    {
        return RET_OUTOFMEM;
    }
    MEMSET( pChipIDInfo_L, 0, sizeof(*pChipIDInfo_L) ); 
    pChipIDInfo_L->chipid_reg_addr = SC031GS_CHIP_ID_LOW_BYTE;
    pChipIDInfo_L->chipid_reg_value = SC031GS_CHIP_ID_LOW_BYTE_DEFAULT;
    ListPrepareItem( pChipIDInfo_L );
    ListAddTail( &pSensorI2cInfo->chipid_info, pChipIDInfo_L );

	//oyyf sensor drv version
	pSensorI2cInfo->sensor_drv_version = CONFIG_SENSOR_DRV_VERSION;
	
    *pdata = pSensorI2cInfo;
	TRACE( SC031GS_INFO, "%s(%d): (exit)\n", __FUNCTION__, __LINE__);
    return RET_SUCCESS;
}

static RESULT SC031GS_IsiSetSensorFrameRateLimit(IsiSensorHandle_t handle, uint32_t minimum_framerate)
{
    SC031GS_Context_t *pSensorCtx = (SC031GS_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( SC031GS_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL ){
        TRACE( SC031GS_ERROR, "%s: pSensorCtx IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
    }

    pSensorCtx->preview_minimum_framerate = minimum_framerate;
    return RET_SUCCESS;
}

/******************************************************************************
 * See header file for detailed comment.
 *****************************************************************************/


/*****************************************************************************/
/**
 */
/*****************************************************************************/
IsiCamDrvConfig_t IsiCamDrvConfig =
{
    0,
    SC031GS_IsiGetSensorIss,
    {
        0,                      /**< IsiSC031GS_t.pszName */
        0,                      /**< IsiSC031GS_t.pRegisterTable */
        0,                      /**< IsiSC031GS_t.pIsiSensorCaps */
        0,						/**< IsiSC031GS_t.pIsiGetSensorIsiVer_t>*/   //oyyf add
        0,                      /**< IsiSC031GS_t.pIsiGetSensorTuningXmlVersion_t>*/   //oyyf add 
        0,                      /**< IsiSC031GS_t.pIsiWhiteBalanceIlluminationChk>*/   //ddl@rock-chips.com 
        0,                      /**< IsiSC031GS_t.pIsiWhiteBalanceIlluminationSet>*/   //ddl@rock-chips.com
        0,                      /**< IsiSC031GS_t.pIsiCheckOTPInfo>*/  //zyc
        0,						/**< IsiSC031GS_t.pIsiSetSensorOTPInfo>*/  //zyl
        0,						/**< IsiSC031GS_t.pIsiEnableSensorOTP>*/  //zyl
        0,                      /**< IsiSC031GS_t.pIsiCreateSensorIss */
        0,                      /**< IsiSC031GS_t.pIsiReleaseSensorIss */
        0,                      /**< IsiSC031GS_t.pIsiGetCapsIss */
        0,                      /**< IsiSC031GS_t.pIsiSetupSensorIss */
        0,                      /**< IsiSC031GS_t.pIsiChangeSensorResolutionIss */
        0,                      /**< IsiSC031GS_t.pIsiSensorSetStreamingIss */
        0,                      /**< IsiSC031GS_t.pIsiSensorSetPowerIss */
        0,                      /**< IsiSC031GS_t.pIsiCheckSensorConnectionIss */
        0,                      /**< IsiSC031GS_t.pIsiGetSensorRevisionIss */
        0,                      /**< IsiSC031GS_t.pIsiRegisterReadIss */
        0,                      /**< IsiSC031GS_t.pIsiRegisterWriteIss */

		0,						/**< IsiSensor_t.pIsiIsEvenFieldIss */
		0,						/**< IsiSensor_t.pIsiGetSensorModeIss */
		0,						/**< IsiSensor_t.pIsiGetSensorFiledStatIss */

        0,                      /**< IsiSC031GS_t.pIsiExposureControlIss */
        0,                      /**< IsiSC031GS_t.pIsiGetGainLimitsIss */
        0,                      /**< IsiSC031GS_t.pIsiGetIntegrationTimeLimitsIss */
        0,                      /**< IsiSC031GS_t.pIsiGetCurrentExposureIss */
        0,                      /**< IsiSC031GS_t.pIsiGetGainIss */
        0,                      /**< IsiSC031GS_t.pIsiGetGainIncrementIss */
        0,                      /**< IsiSC031GS_t.pIsiSetGainIss */
        0,                      /**< IsiSC031GS_t.pIsiGetIntegrationTimeIss */
        0,                      /**< IsiSC031GS_t.pIsiGetIntegrationTimeIncrementIss */
        0,                      /**< IsiSC031GS_t.pIsiSetIntegrationTimeIss */
        0,                      /**< IsiSC031GS_t.pIsiGetResolutionIss */
        0,                      /**< IsiSC031GS_t.pIsiGetAfpsInfoIss */

        0,                      /**< IsiSC031GS_t.pIsiGetCalibKFactor */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibPcaMatrix */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibSvdMeanValue */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibCenterLine */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibClipParam */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibGlobalFadeParam */
        0,                      /**< IsiSC031GS_t.pIsiGetCalibFadeParam */
        0,                      /**< IsiSC031GS_t.pIsiGetIlluProfile */
        0,                      /**< IsiSC031GS_t.pIsiGetLscMatrixTable */

        0,                      /**< IsiSC031GS_t.pIsiMdiInitMotoDriveMds */
        0,                      /**< IsiSC031GS_t.pIsiMdiSetupMotoDrive */
        0,                      /**< IsiSC031GS_t.pIsiMdiFocusSet */
        0,                      /**< IsiSC031GS_t.pIsiMdiFocusGet */
        0,                      /**< IsiSC031GS_t.pIsiMdiFocusCalibrate */

        0,                      /**< IsiSC031GS_t.pIsiGetSensorMipiInfoIss */

        0,                      /**< IsiSC031GS_t.pIsiActivateTestPattern */
        0,
        0,						/**< IsiSC031GS_t.pIsiGetColorIss */
    },
    SC031GS_IsiGetSensorI2cInfo,
};


