//SC031GS_MIPI_priv.h
/*****************************************************************************/
/*!
 *  \file        SC031GS_priv.h \n
 *  \version     1.0 \n
 *  \author      Meinicke \n
 *  \brief       Private header file for sensor specific code of the OV13850. \n
 *
 *  \revision    $Revision: 432 $ \n
 *               $Author: neugebaa $ \n
 *               $Date: 2009-06-30 11:48:59 +0200 (Di, 30 Jun 2009) $ \n
 *               $Id: OV13850_priv.h 432 2009-06-30 09:48:59Z neugebaa $ \n
 */
/*  This is an unpublished work, the copyright in which vests in Silicon Image
 *  GmbH. The information contained herein is the property of Silicon Image GmbH
 *  and is supplied without liability for errors or omissions. No part may be
 *  reproduced or used expect as authorized by contract or other written
 *  permission. Copyright(c) Silicon Image GmbH, 2009, all rights reserved.
 */
/*****************************************************************************/

/*Modify by wpzz@rock-chips.com*/
/*
#ifndef _SC031GS_PRIV_H
#define _SC031GS_PRIV_H

#include "isi_priv.h"

#if( SC031GS_DRIVER_USAGE == USE_CAM_DRV_EN )
*/


#ifndef __SC031GS_PRIV_H__
#define __SC031GS_PRIV_H__

#include <ebase/types.h>
#include <common/return_codes.h>
#include <hal/hal_api.h>



#ifdef __cplusplus
extern "C"
{
#endif

/*
*              SILICONIMAGE LIBISP VERSION NOTE
*
*v0.1.0 : create file --wpzz
*/

#define CONFIG_SENSOR_DRV_VERSION KERNEL_VERSION(0, 1, 0)

#define SC031GS_CHIP_ID_HIGH_BYTE            (0x3107) // r - 
#define SC031GS_CHIP_ID_LOW_BYTE             (0x3108) // r - 

#define SC031GS_CHIP_ID_HIGH_BYTE_DEFAULT    (0x00) // r - 
#define SC031GS_CHIP_ID_LOW_BYTE_DEFAULT     (0x31) // r - 

#define SC031GS_MODE_SELECT                  (0x0100)

#define SC031GS_SOFTWARE_RST                 (0x0103) // rw - Bit[7:1]not used  Bit[0]software_reset
#define SC031GS_SOFTWARE_RST_VALUE			(0x01)

typedef struct SC031GS_VcmInfo_s                 /* ddl@rock-chips.com: v0.3.0 */
{
    uint32_t StartCurrent;
    uint32_t RatedCurrent;
    uint32_t Step;
    uint32_t StepMode;
} SC031GS_VcmInfo_t;

typedef struct SC031GS_Context_s
{
    IsiSensorContext_t  IsiCtx;                 /**< common context of ISI and ISI driver layer; @note: MUST BE FIRST IN DRIVER CONTEXT */

    //// modify below here ////

    IsiSensorConfig_t   Config;                 /**< sensor configuration */
    bool_t              Configured;             /**< flags that config was applied to sensor */
    bool_t              Streaming;              /**< flags that sensor is streaming data */
    bool_t              TestPattern;            /**< flags that sensor is streaming test-pattern */

    bool_t              isAfpsRun;              /**< if true, just do anything required for Afps parameter calculation, but DON'T access SensorHW! */

    bool_t              GroupHold;

    float               VtPixClkFreq;           /**< pixel clock */
    uint16_t            LineLengthPck;          /**< line length with blanking */
    uint16_t            FrameLengthLines;       /**< frame line length */

    float               AecMaxGain;
    float               AecMinGain;
    float               AecMaxIntegrationTime;
    float               AecMinIntegrationTime;

    float               AecIntegrationTimeIncrement; /**< _smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application) */
    float               AecGainIncrement;            /**< _smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application) */

    float               AecCurGain;
    float               AecCurIntegrationTime;

    uint16_t            OldGain;               /**< gain multiplier */
    uint32_t            OldCoarseIntegrationTime;
    uint32_t            OldFineIntegrationTime;

    IsiSensorMipiInfo   IsiSensorMipiInfo;
	SC031GS_VcmInfo_t    VcmInfo;
	uint32_t			preview_minimum_framerate;
} SC031GS_Context_t;

#ifdef __cplusplus
}
#endif

#endif

