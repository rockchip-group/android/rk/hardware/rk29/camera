//SC031GS_tables.c
/*****************************************************************************/
/*!
 *  \file        SC031GS_tables.c \n
 *  \version     1.0 \n
 *  \author      Meinicke \n
 *  \brief       Image-sensor-specific tables and other
 *               constant values/structures for OV13850. \n
 *
 *  \revision    $Revision: 803 $ \n
 *               $Author: $ \n
 *               $Date: 2010-02-26 16:35:22 +0100 (Fr, 26 Feb 2010) $ \n
 *               $Id: SC031GS_tables.c 803 2010-02-26 15:35:22Z  $ \n
 */
/*  This is an unpublished work, the copyright in which vests in Silicon Image
 *  GmbH. The information contained herein is the property of Silicon Image GmbH
 *  and is supplied without liability for errors or omissions. No part may be
 *  reproduced or used expect as authorized by contract or other written
 *  permission. Copyright(c) Silicon Image GmbH, 2009, all rights reserved.
 */
/*****************************************************************************/
/*
#include "stdinc.h"

#if( SC031GS_DRIVER_USAGE == USE_CAM_DRV_EN )
*/


#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"
#include "SC031GS_MIPI_priv.h"


/*****************************************************************************
 * DEFINES
 *****************************************************************************/


/*****************************************************************************
 * GLOBALS
 *****************************************************************************/

// Image sensor register settings default values taken from data sheet SC031GS_DS_1.1.pdf.
// The settings may be altered by the code in IsiSetupSensor.

//one lane
const IsiRegDescription_t SC031GS_g_aRegDescription[] =
{
	{0x0100, 0x00,"",eReadWrite},
	{0x300f, 0x0f,"",eReadWrite},
	{0x3018, 0x1f,"",eReadWrite},
	{0x3019, 0xff,"",eReadWrite},
	{0x301c, 0xb4,"",eReadWrite},
	{0x320c, 0x03,"",eReadWrite},
	{0x320d, 0x6e,"",eReadWrite},
	//{0x320e, 0x02}, //120fps
	//{0x320f, 0xab},
	{0x320e, 0x0a,"",eReadWrite},	//30fps
	{0x320f, 0xac,"",eReadWrite},
	{0x3250, 0xf0,"",eReadWrite},
	{0x3251, 0x02,"",eReadWrite},
	{0x3252, 0x02,"",eReadWrite},
	{0x3253, 0xa6,"",eReadWrite},
	{0x3254, 0x02,"",eReadWrite},
	{0x3255, 0x07,"",eReadWrite},
	{0x3304, 0x48,"",eReadWrite},
	{0x3306, 0x38,"",eReadWrite},
	{0x3309, 0x68,"",eReadWrite},
	{0x330b, 0xe0,"",eReadWrite},
	{0x330c, 0x18,"",eReadWrite},
	{0x330f, 0x20,"",eReadWrite},
	{0x3310, 0x10,"",eReadWrite},
	{0x3314, 0x3a,"",eReadWrite},
	{0x3315, 0x38,"",eReadWrite},
	{0x3316, 0x48,"",eReadWrite},
	{0x3317, 0x20,"",eReadWrite},
	{0x3329, 0x3c,"",eReadWrite},
	{0x332d, 0x3c,"",eReadWrite},
	{0x332f, 0x40,"",eReadWrite},
	{0x3335, 0x44,"",eReadWrite},
	{0x3344, 0x44,"",eReadWrite},
	{0x335b, 0x80,"",eReadWrite},
	{0x335f, 0x80,"",eReadWrite},
	{0x3366, 0x06,"",eReadWrite},
	{0x3385, 0x31,"",eReadWrite},
	{0x3387, 0x51,"",eReadWrite},
	{0x3389, 0x01,"",eReadWrite},
	{0x33b1, 0x03,"",eReadWrite},
	{0x33b2, 0x06,"",eReadWrite},
	{0x3621, 0xa4,"",eReadWrite},
	{0x3622, 0x05,"",eReadWrite},
	{0x3624, 0x47,"",eReadWrite},
	{0x3630, 0x46,"",eReadWrite},
	{0x3631, 0x48,"",eReadWrite},
	{0x3633, 0x52,"",eReadWrite},
	{0x3636, 0x25,"",eReadWrite},
	{0x3637, 0x89,"",eReadWrite},
	{0x3638, 0x0f,"",eReadWrite},
	{0x3639, 0x08,"",eReadWrite},
	{0x363a, 0x00,"",eReadWrite},
	{0x363b, 0x48,"",eReadWrite},
	{0x363c, 0x06,"",eReadWrite},
	{0x363d, 0x00,"",eReadWrite},
	{0x363e, 0xf8,"",eReadWrite},
	{0x3640, 0x02,"",eReadWrite},
	{0x3641, 0x03,"",eReadWrite},  //driver cap  0x01  0x00
	{0x36e9, 0x00,"",eReadWrite},
	{0x36ea, 0x3b,"",eReadWrite},
	{0x36eb, 0x1a,"",eReadWrite},
	{0x36ec, 0x0a,"",eReadWrite},
	{0x36ed, 0x33,"",eReadWrite},
	{0x36f9, 0x00,"",eReadWrite},
	{0x36fa, 0x3a,"",eReadWrite},
	{0x36fc, 0x01,"",eReadWrite},
	{0x3908, 0x91,"",eReadWrite},

	//{0x3d01, 0x0a,"",eReadWrite},  //fsync length

	{0x3d08, 0x00,"",eReadWrite},  // 0x00	//0x01

	{0x3e01, 0xd0,"",eReadWrite},
	{0x3e02, 0xff,"",eReadWrite},
	{0x3e06, 0x0c,"",eReadWrite},
	{0x4500, 0x59,"",eReadWrite},
	{0x4501, 0xc4,"",eReadWrite},
	{0x5011, 0x00,"",eReadWrite},
//	{0x0100, 0x01},
	{0x4418, 0x08,"",eReadWrite},
	{0x4419, 0x8a,"",eReadWrite},
//	test pattern
//	{0x4501, 0xac},
//	{0x5011, 0x01},

	/*gain*/
	{0x3e02, 0x03,"",eReadWrite},//0x03, {16’h3e08,16’h3e09}/8’h10

	/*group hold*/
	{0x3802, 0x00,"",eReadWrite}, //time delay
	{0x3235, 0x00,"",eReadWrite}, //time delay
	{0x3236, 0x00,"",eReadWrite}, //time delay

    {0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t SC031GS_g_svga[] =
{
    {0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t SC031GS_g_640x480[] =
{
    {0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t SC031GS_g_640x480_30fps[] =
{
	{0x320e, 0x0a,"",eReadWrite},	//30fps
	{0x320f, 0xac,"",eReadWrite},
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t SC031GS_g_640x480_20fps[] =
{
	{0x320e, 0x10,"",eReadWrite},	//20fps
	{0x320f, 0x02,"",eReadWrite},
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};
const IsiRegDescription_t SC031GS_g_640x480_15fps[] =
{
	{0x320e, 0x15,"",eReadWrite},	//15fps
	{0x320f, 0x58,"",eReadWrite},
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t SC031GS_g_640x480_10fps[] =
{
	{0x320e, 0x20,"",eReadWrite},	//10fps
	{0x320f, 0x04,"",eReadWrite},
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};
