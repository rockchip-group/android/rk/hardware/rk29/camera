//OV2735_tables.c
/*****************************************************************************/
/*!
 *  \file        OV2735_tables.c \n
 *  \version     1.0 \n
 *  \author      Meinicke \n
 *  \brief       Image-sensor-specific tables and other
 *               constant values/structures for OV13850. \n
 *
 *  \revision    $Revision: 803 $ \n
 *               $Author: $ \n
 *               $Date: 2010-02-26 16:35:22 +0100 (Fr, 26 Feb 2010) $ \n
 *               $Id: OV13850_tables.c 803 2010-02-26 15:35:22Z  $ \n
 */
/*  This is an unpublished work, the copyright in which vests in Silicon Image
 *  GmbH. The information contained herein is the property of Silicon Image GmbH
 *  and is supplied without liability for errors or omissions. No part may be
 *  reproduced or used expect as authorized by contract or other written
 *  permission. Copyright(c) Silicon Image GmbH, 2009, all rights reserved.
 */
/*****************************************************************************/
/*
#include "stdinc.h"

#if( OV2735_DRIVER_USAGE == USE_CAM_DRV_EN )
*/


#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"
#include "OV2735_MIPI_priv.h"


/*****************************************************************************
 * DEFINES
 *****************************************************************************/


/*****************************************************************************
 * GLOBALS
 *****************************************************************************/
// The settings may be altered by the code in IsiSetupSensor.

//one lane
const IsiRegDescription_t Sensor_g_aRegDescription[] =
{
	{0xfd, 0x00, "eReadWrite", eReadWrite},
	{0x20, 0x01, "eReadWrite", eReadWrite}, 	// soft reset modify to 0x00
	{0x0 , 0x3 , "eDelay", eDelay},			// delay 3ms
	{0xfd, 0x00, "eReadWrite", eReadWrite},
	{0x2f, 0x10, "eReadWrite", eReadWrite}, // clk and pll setting
	{0x34, 0x00, "eReadWrite", eReadWrite},
	{0x30, 0x15, "eReadWrite", eReadWrite},
	{0x33, 0x01, "eReadWrite", eReadWrite},
	{0x35, 0x20, "eReadWrite", eReadWrite},
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x00, "eReadWrite", eReadWrite}, // disalbe modify VTS
	{0x30, 0x00, "eReadWrite", eReadWrite},
	{0x03, 0x01, "eReadWrite", eReadWrite}, // exposure time
	{0x04, 0x8f, "eReadWrite", eReadWrite},
	{0x01, 0x01, "eReadWrite", eReadWrite}, // enable of frame sync signal 
	{0x09, 0x00, "eReadWrite", eReadWrite}, // HBLANK
	{0x0a, 0x20, "eReadWrite", eReadWrite},
	{0x06, 0x0a, "eReadWrite", eReadWrite}, // VBLANK 8LSB
	{0x24, 0x10, "eReadWrite", eReadWrite},
	{0x01, 0x01, "eReadWrite", eReadWrite},
	{0xfb, 0x73, "eReadWrite", eReadWrite}, // ABL
	{0x01, 0x01, "eReadWrite", eReadWrite},
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x1a, 0x6b, "eReadWrite", eReadWrite}, // Timing ctrl
	{0x1c, 0xea, "eReadWrite", eReadWrite},
	{0x16, 0x0c, "eReadWrite", eReadWrite},
	{0x21, 0x00, "eReadWrite", eReadWrite},
	{0x11, 0x63, "eReadWrite", eReadWrite},
	{0x19, 0xc3, "eReadWrite", eReadWrite},
	{0x26, 0x5a, "eReadWrite", eReadWrite}, // ANALOG CTRL
	{0x29, 0x01, "eReadWrite", eReadWrite},
	{0x33, 0x6f, "eReadWrite", eReadWrite},
	{0x2a, 0xd2, "eReadWrite", eReadWrite},
	{0x2c, 0x40, "eReadWrite", eReadWrite},
	{0xd0, 0x02, "eReadWrite", eReadWrite},
	{0xd1, 0x01, "eReadWrite", eReadWrite},
	{0xd2, 0x20, "eReadWrite", eReadWrite},
	{0xd3, 0x04, "eReadWrite", eReadWrite},
	{0xd4, 0x2a, "eReadWrite", eReadWrite},
	{0x50, 0x00, "eReadWrite", eReadWrite}, // Timing ctrl
	{0x51, 0x2c, "eReadWrite", eReadWrite},
	{0x52, 0x29, "eReadWrite", eReadWrite},
	{0x53, 0x00, "eReadWrite", eReadWrite},
	{0x55, 0x44, "eReadWrite", eReadWrite},
	{0x58, 0x29, "eReadWrite", eReadWrite},
	{0x5a, 0x00, "eReadWrite", eReadWrite},
	{0x5b, 0x00, "eReadWrite", eReadWrite},
	{0x5d, 0x00, "eReadWrite", eReadWrite},
	{0x64, 0x2f, "eReadWrite", eReadWrite},
	{0x66, 0x62, "eReadWrite", eReadWrite},
	{0x68, 0x5b, "eReadWrite", eReadWrite},
	{0x75, 0x46, "eReadWrite", eReadWrite},
	{0x76, 0x36, "eReadWrite", eReadWrite},
	{0x77, 0x4f, "eReadWrite", eReadWrite},
	{0x78, 0xef, "eReadWrite", eReadWrite},
	{0x72, 0xcf, "eReadWrite", eReadWrite},
	{0x73, 0x36, "eReadWrite", eReadWrite},
	{0x7d, 0x0d, "eReadWrite", eReadWrite},
	{0x7e, 0x0d, "eReadWrite", eReadWrite},
	{0x8a, 0x77, "eReadWrite", eReadWrite},
	{0x8b, 0x77, "eReadWrite", eReadWrite},
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0xb1, 0x83, "eReadWrite", eReadWrite}, // MIPI register ---
	{0xb3, 0x0b, "eReadWrite", eReadWrite},
	{0xb4, 0x14, "eReadWrite", eReadWrite},
	{0x9d, 0x40, "eReadWrite", eReadWrite},
	{0xa1, 0x05, "eReadWrite", eReadWrite},
	{0x94, 0x44, "eReadWrite", eReadWrite},
	{0x95, 0x33, "eReadWrite", eReadWrite},
	{0x96, 0x1f, "eReadWrite", eReadWrite},
	{0x98, 0x45, "eReadWrite", eReadWrite},
	{0x9c, 0x10, "eReadWrite", eReadWrite},
	{0xb5, 0x70, "eReadWrite", eReadWrite},
	// {0xa0, 0x01, "eReadWrite", eReadWrite}, // mipi en buf ---
	{0x25, 0xe0, "eReadWrite", eReadWrite},
	{0x20, 0x7b, "eReadWrite", eReadWrite},
	{0x8f, 0x88, "eReadWrite", eReadWrite}, // H_SIZE_MIPI_8LSB
	{0x91, 0x40, "eReadWrite", eReadWrite}, // V_SIZE_MIPI_8LSB
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0xfd, 0x02, "eReadWrite", eReadWrite},
	{0xa1, 0x04, "eReadWrite", eReadWrite},
	{0xa3, 0x40, "eReadWrite", eReadWrite},
	{0xa5, 0x02, "eReadWrite", eReadWrite},
	{0xa7, 0xc4, "eReadWrite", eReadWrite},
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x86, 0x77, "eReadWrite", eReadWrite}, // BLC
	{0x89, 0x77, "eReadWrite", eReadWrite},
	{0x87, 0x74, "eReadWrite", eReadWrite},
	{0x88, 0x74, "eReadWrite", eReadWrite},
	{0xfc, 0xe0, "eReadWrite", eReadWrite},
	{0xfe, 0xe0, "eReadWrite", eReadWrite},
	{0xf0, 0x40, "eReadWrite", eReadWrite},
	{0xf1, 0x40, "eReadWrite", eReadWrite},
	{0xf2, 0x40, "eReadWrite", eReadWrite},
	{0xf3, 0x40, "eReadWrite", eReadWrite},
	{0x00, 0x00, "eReadWrite",eTableEnd}
};

const IsiRegDescription_t Sensor_g_TV1080[] =
{
	{0xfd, 0x02, "eReadWrite", eReadWrite},	
	{0xa0, 0x00, "eReadWrite", eReadWrite},	// Image vertical start MSB3bits
	{0xa1, 0x08, "eReadWrite", eReadWrite},	// Image vertical start LSB8bits
	{0xa2, 0x04, "eReadWrite", eReadWrite},	// image vertical size  MSB8bits
	{0xa3, 0x38, "eReadWrite", eReadWrite},	// image vertical size  LSB8bits
	{0xa4, 0x00, "eReadWrite", eReadWrite},
	{0xa5, 0x08, "eReadWrite", eReadWrite},	// H start 8Lsb
	{0xa6, 0x03, "eReadWrite", eReadWrite},
	{0xa7, 0xc0, "eReadWrite", eReadWrite},	// Half H size Lsb8bits
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x8e, 0x07, "eReadWrite", eReadWrite},
	{0x8f, 0x80, "eReadWrite", eReadWrite},	// MIPI column number
	{0x90, 0x04, "eReadWrite", eReadWrite},	// MIPI row number
	{0x91, 0x38, "eReadWrite", eReadWrite},
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_30fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x04, "eReadWrite", eReadWrite},
	{0x0f, 0xc1, "eReadWrite", eReadWrite},	// Vblank, VTS:0x4c1, 30.037fps
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_25fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x05, "eReadWrite", eReadWrite},
	{0x0f, 0xB6, "eReadWrite", eReadWrite},	// Vblank, VTS:0x5B6
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_20fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x07, "eReadWrite", eReadWrite},
	{0x0f, 0x23, "eReadWrite", eReadWrite},	// Vblank, VTS:0x723
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_15fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x09, "eReadWrite", eReadWrite},
	{0x0f, 0x85, "eReadWrite", eReadWrite},	// Vblank, VTS:0x985
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_10fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x0E, "eReadWrite", eReadWrite},
	{0x0f, 0x47, "eReadWrite", eReadWrite},	// Vblank, VTS:0xE47
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};
const IsiRegDescription_t Sensor_g_TV1080_5fps[] =
{
	{0xfd, 0x01, "eReadWrite", eReadWrite},
	{0x0d, 0x10, "eReadWrite", eReadWrite},	// enable manual modify the VTS
	{0x0e, 0x1C, "eReadWrite", eReadWrite},
	{0x0f, 0x8F, "eReadWrite", eReadWrite},	// Vblank, VTS:0x1C8F
	{0x01, 0x01, "eReadWrite", eReadWrite},	// enable of frame sync signal
	{0x00, 0x00, "eReadWrite",eTableEnd}
};


